﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="srResourceDetail.aspx.cs" Inherits="srResourceDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" Runat="Server">
    <asp:Panel ID="pnlSRresources" runat="server">
        <div class="mpeDiv" style="padding-top: 300px;padding-left:30px; padding-right:30px;">
            <div class="mpeDivHeader">
                Resource Details
                <span class="closeit">
                    <asp:LinkButton ID="lbtnClose" runat="server" Text="Close" CssClass="btn btn-grantee" OnClick="btnRscsClose_Click" />
                    
					
                </span>
            </div>
            <div class="popContent">
                <asp:Literal ID="ltlResourceDetails" runat="server"></asp:Literal>
                
                <div class="col-lg-12 col-xs-12">
                <asp:Button ID="btnRscsClose" runat="server" CssClass="btn btn-grantee" Text="Close Window" OnClick="btnRscsClose_Click" />
				</div>
            </div>

                        

        </div>
    </asp:Panel>
</asp:Content>

