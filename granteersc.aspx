﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="granteersc.aspx.cs" Inherits="granteersc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" Runat="Server">
    
   <section class="home-features" style="padding-top:300px;">
      <div class="container">
    <h1>Grantee Resource</h1>
    <table>
        <tr>
            <th>
                Grantee:
            </th>
            <td>
                <asp:DropDownList ID="ddlGrantees" runat="server" AppendDataBoundItems="True" DataTextField="Grantee" DataValueField="ID">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlGrantees" InitialValue="" Display="Dynamic" ForeColor="Red" ErrorMessage="This field is required."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <th>
                Topic:
            </th>
            <td>
                <asp:DropDownList ID="ddlTopic" runat="server" AppendDataBoundItems="true" DataSourceID="SqlDataSource2" DataTextField="topicname" DataValueField="id" >
                    <asp:ListItem Value="">Select One</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTopic" InitialValue="" Display="Dynamic" ForeColor="Red" ErrorMessage="This field is required."></asp:RequiredFieldValidator>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:StrivingReaderConnectionString %>" SelectCommand="SELECT [id], [topicname] FROM [GranteeTopic]"></asp:SqlDataSource>
                
            </td>
        </tr>
        <tr>
            <th>
                Resource Name:
            </th>
            <td>
                <asp:TextBox ID="txtRscName" runat="server" Width="680"></asp:TextBox>
            </td>
        </tr>
        <tr>
             <th>
                Resource Description:
            </th>
             <td>
                <asp:TextBox ID="txtRscDes" runat="server" Width="680" Height="120" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Uplaod document:
            </td>
             <td>
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="btn btn-grantee" />&nbsp;&nbsp;
                 <asp:Label ID="lblFileName" runat="server" Text="Label"></asp:Label>
                </td>
        </tr>
        <tr>
            <th>URL:</th>
            <td>
                 <asp:TextBox ID="txtURL" runat="server" Width="680"></asp:TextBox>
            </td>
        </tr>
        <tr id="rwActive" runat="server">
            <th>Approved:</th>
            <td>
                <asp:CheckBox ID="cbxIsActive" runat="server"  /></td>
        </tr>
    </table>
    <p >
        <asp:Button ID="Submit" runat="server" CssClass="btn btn-grantee" Text="Submit" OnClick="Submit_Click"/>
        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-grantee" Text="Close" CausesValidation="false" OnClick="btnClose_Click" />
    </p>
    <asp:HiddenField ID="hfID" runat="server" />
        </div>
    </section>
</asp:Content>

