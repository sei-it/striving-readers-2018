﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class granteeDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(Session["grnteeID"]);
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            grantee rcd = db.grantees.SingleOrDefault(x => x.ID == id && x.isActive);
            ltlgranteename.Text = rcd.Grantee1;
            ltlGState.Text = rcd.State;
            ltlDescription.Text = rcd.Abstract;
            ltlContactName.Text = rcd.ContactName;
            ltlEmail.Text = rcd.ContactEmail;
            //ltlPhone.Text = rcd.ContactPhone;
        }
    }


    protected void btnGrntClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("grantees.aspx");
    }
}