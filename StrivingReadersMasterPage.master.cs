﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StrivingReadersMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        SetBodyCssClass();
    }

    private void SetBodyCssClass()
    {
        string pageType = Page.GetType().Name;
        pageType = pageType.Replace("_aspx", string.Empty);
        pageType = pageType.Replace('_', '-');
        string bodyClass = SRBody.Attributes["class"] + " " + pageType;
        SRBody.Attributes["class"] = bodyClass.Trim();
    }
}
