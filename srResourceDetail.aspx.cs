﻿using Synergy.StrivingReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class srResourceDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int id = Request.QueryString["srID"]==null?0 : Convert.ToInt32(Request.QueryString["srID"]);
        string sentence = Request.QueryString["sentence"]==null? "" : Request.QueryString["sentence"];
        string styletype = Request.QueryString["styletype"]==null? "" : Request.QueryString["styletype"];
        string topic = Request.QueryString["topic"]==null? "" : Request.QueryString["topic"];
        ltlResourceDetails.Text = "No Contents.";
        
        var data = ManageUtility.SearchKeyword(sentence, styletype, topic);
        foreach (var itm in data)
        {
            if (itm.ID == id)
            {
                ltlResourceDetails.Text = itm.DisplayData;
                break;
            }
        }
    }

    protected void btnRscsClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("srResources.aspx");
    }
}