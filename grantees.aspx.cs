﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class grantees : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string statecode = Request.QueryString["state"]==null? "" : Request.QueryString["state"].ToString();

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            string statename = "";
            if(statecode!="")
                statename = db.States.SingleOrDefault(x => x.StateAbb == statecode).StateFullName;

            var rcd = from itms in db.grantees
                      where itms.isActive == true && itms.State == statename
                      orderby itms.State
                      select itms;

            GridView2.DataSource = rcd;
            GridView2.DataBind();

        }

    }

    protected void ShowDetails(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Session["grnteeID"] = id;
        Response.Redirect("granteeDetail.aspx");
    }
}