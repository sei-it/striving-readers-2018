﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="webinars-upcoming.aspx.cs" Inherits="webinars_upcoming" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Striving Readers - Webinars</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" Runat="Server">
    <section class="head-image">
                    <img src="images/webinar-banner.jpg" class="img-responsive" alt="Group people attending a webinar" />
                </section>

                <section class="bg-blue center home-section-pad">
                    <div class="container">
                        <h2>Upcoming CoP Events</h2>
                    </div>
                </section>

				<section class="internal-page-content">
					<div class="container">
						<div class="col-lg-12 col-xs-12">
							<h2>Webinar Topics</h2>
							<p>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. Ei admodum temporibus deterruisset usu, duo iuvaret volutpat delicatissimi ut, ut has sanctus rationibus concludaturque. Intellegat eloquentiam adversarium vix no. Mei bonorum definitiones ea, sea an dolor affert laoreet.</p>
					<asp:GridView ID="gvUpcommingEvents" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal" AllowSorting="true" 
                        ShowHeaderWhenEmpty="true" OnSorting="gvUpcommingEvents_Sorting"  >
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
        
                        <Columns>
                             <asp:BoundField DataField="Topic" SortExpression="" HeaderText="Topic" />
                             <asp:TemplateField HeaderText="Description">
                                 <ItemTemplate>
                                     <asp:Literal ID="Literal1" runat="server" Text='<%# getDesShort((int)Eval("id"))%>'></asp:Literal>
                                 </ItemTemplate>
                             </asp:TemplateField>
                            <asp:TemplateField HeaderText="Instructor/Date">
                                 <ItemTemplate>
                                     <asp:Literal ID="Literal2" runat="server" Text='<%# getInstructor_Date((int)Eval("id"))%>'></asp:Literal>
                                 </ItemTemplate>
                             </asp:TemplateField>
                            <%--<asp:BoundField HeaderText="Description" DataField='<% getDesShort(Eval("id"))%>' HtmlEncode="false" />
                            <asp:BoundField HeaderText="Instructor/Date" DataField='<% getInstructor_Date(Eval("id"))%>' HtmlEncode="false" />--%>
                        </Columns>
                    </asp:GridView>
						</div>
						
					</div>
				</section>      
</asp:Content>

