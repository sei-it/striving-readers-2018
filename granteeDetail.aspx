﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="granteeDetail.aspx.cs" Inherits="granteeDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" Runat="Server">
    <asp:Panel ID="pnlSRgrantee" runat="server">
    <div class="mpeDiv" style="padding-top: 300px;padding-left:30px; padding-right:30px;">
            <div class="mpeDivHeader">
                Grantee Details
                <span class="closeit"><asp:LinkButton ID="lbtnClose" runat="server" Text="Close" CssClass="btn btn-grantee" OnClick="btnGrntClose_Click" />
                </span>
            </div>
            <div class="popContent">
                <table >
                     <tr>
						<th >Grantee Name:</th>
                        <td>
                            <asp:Literal ID="ltlgranteename" runat="server" />
                        </td>
                    </tr>
					<tr >
						<th >
                            Grantee State:
						</th>
						<td>
                            <asp:Literal ID="ltlGState" runat="server"/>
						</td>
					</tr>
                    <tr>
						<th>
                            Grantee Description:
						</th>
						<td>
                            <asp:Literal ID="ltlDescription" runat="server"/>
						</td>
					</tr>
                     <tr>
						<th>
                            Contact Name:
						</th>
						<td>
                            <asp:Literal ID="ltlContactName" runat="server"/>
						</td>
					</tr>
                    <tr>
                        <th>Contact Email: </th>
                        <td>
                            <asp:Literal ID="ltlEmail" runat="server" />
                        </td>
                     <%-- <tr>
						<th>
                            Contact Phone:
						</th>
						<td>
                            <asp:Literal ID="ltlPhone" runat="server"/>
						</td>
					</tr>--%>
                     <tr>
                         <td colspan="2">
                             <asp:Button ID="btnGrntClose" runat="server" CssClass="btn btn-grantee" Text="Close Window" OnClick="btnGrntClose_Click" />
                         </td>
                     </tr>

                </table>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

