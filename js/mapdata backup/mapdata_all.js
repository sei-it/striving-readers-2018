var simplemaps_usmap_mapdata = {

	main_settings:{
		//General settings
		width: 'responsive', //or 'responsive'
		background_color: '#FFFFFF',	
		background_transparent: 'yes',
		border_color: '#000000',
		popups: 'off', //on_click, on_hover, or detect
	
		//State defaults
		state_description:   '',
		state_color: '#88A4BC',
		state_hover_color: '#3B729F',
		state_url: '#',
		border_size: .5,		
		all_states_inactive: 'yes',
		all_states_zoomable: 'no',		
		
		//Location defaults
		location_description:  'Location description',
		location_color: '#FF0067',
		location_opacity: .8,
		location_hover_opacity: 1,
		location_url: '',
		location_size: 25,
		location_type: 'square', // circle, square, image
		location_image_source: 'frog.png', //name of image in the map_images folder		
		location_border_color: '#FFFFFF',
		location_border: 2,
		location_hover_border: 2.5,				
		all_locations_inactive: 'no',
		all_locations_hidden: 'no',
		
		//Labels
		label_color: '#d5ddec',	
		label_hover_color: '#d5ddec',		
		label_size: 22,
		label_font: 'Arial',
		hide_labels: 'no',
		hide_eastern_labels: 'no',
		
		//Zoom settings
		zoom: 'yes', //use default regions
		back_image: 'no',   //Use image instead of arrow for back zoom				
		arrow_color: '#3B729F',
		arrow_color_border: '#88A4BC',
		initial_back: 'no', //Show back button when zoomed out and do this JavaScript upon click		
		initial_zoom: -1,  //-1 is zoomed out, 0 is for the first continent etc	
		initial_zoom_solo: 'no', //hide adjacent states when starting map zoomed in
		region_opacity: 1,
		region_hover_opacity: .6,
		zoom_out_incrementally: 'yes',  // if no, map will zoom all the way out on click
		zoom_percentage: .99,
		zoom_time: .5, //time to zoom between regions in seconds
		
		//Popup settings
		popup_color: 'white',
		popup_opacity: .9,
		popup_shadow: 1,
		popup_corners: 5,
		popup_font: '12px/1.5 Verdana, Arial, Helvetica, sans-serif',
		popup_nocss: 'no', //use your own css	
		
		//Advanced settings
		div: 'map',
		auto_load: 'yes',		
		url_new_tab: 'no', 
		images_directory: 'default', //e.g. 'map_images/'
		fade_time:  .1, //time to fade out		
		link_text: '(Link)'  //Text mobile browsers will see for links	
		
	},

	state_specific:{	
		"HI": {
			name: 'Hawaii',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=HI' //replace 'default' if active	
		},
		"AK": {
			name: 'Alaska',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=AK' //replace 'default' if active	
		},
		"FL": {
			name: 'Florida',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=Florida' //replace 'default' if active	
			},
		"NH": {
			name: 'New Hampshire',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=NH' //replace 'default' if active
			},
		"VT": {
			name: 'Vermont',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=VT' //replace 'default' if active
			},
		"ME": {
			name: 'Maine',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=ME' //replace 'default' if active
			},
		"RI": {
			name: 'Rhode Island',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=RI' //replace 'default' if active
			},
		"NY": {
			name: 'New York',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=NY' //replace 'default' if active
		},
		"PA": {
			name: 'Pennsylvania',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=PA' //replace 'default' if active				
			},
		"NJ": {
			name: 'New Jersey',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=NJ' //replace 'default' if active					
			},
		"DE": {
			name: 'Delaware',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=DE' //replace 'default' if active				
			},
		"MD": {
			name: 'Maryland',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=MD' //replace 'default' if active							
			},
		"VA": {
			name: 'Virginia',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=VA' //replace 'default' if active				
			},
		"WV": {
			name: 'West Virginia',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=WV' //replace 'default' if active				
			},
		"OH": {
			name: 'Ohio',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=OH' //replace 'default' if active			
			},
		"IN": {
			name: 'Indiana',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=IN' //replace 'default' if active					
			},
		"IL": {
			name: 'Illinois',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=IL' //replace 'default' if active				
			},
		"CT": {
			name: 'Connecticut',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=CT' //replace 'default' if active					
			},
		"WI": {
			name: 'Wisconsin',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=WI' //replace 'default' if active				
			},
		"NC": {
			name: 'North Carolina',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=NC' //replace 'default' if active				
			},
		"DC": {
			name: 'District of Columbia',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=DC' //replace 'default' if active	
		},
		"MA": {
			name: 'Massachusetts',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=MA' //replace 'default' if active					
			},
		"TN": {
			name: 'Tennessee',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=TN' //replace 'default' if active			
			},
		"AR": {
			name: 'Arkansas',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=AR' //replace 'default' if active				
			},
		"MO": {
			name: 'Missouri',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=MO' //replace 'default' if active				
			},
		"GA": {
			name: 'Georgia',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=GA' //replace 'default' if active				
			},
		"SC": {
			name: 'South Carolina',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=SC' //replace 'default' if active				
			},
		"KY": {
			name: 'Kentucky',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=KY' //replace 'default' if active			
			},
		"AL": {
			name: 'Alabama',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=AL' //replace 'default' if active						
			},
		"LA": {
			name: 'Louisiana',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=LA' //replace 'default' if active				
			},
		"MS": {
			name: 'Mississippi',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=MS' //replace 'default' if active					
			},
		"IA": {
			name: 'Iowa',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=IA' //replace 'default' if active				
			},
		"MN": {
			name: 'Minnesota',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=MN' //replace 'default' if active	
			},
		"OK": {
			name: 'Oklahoma',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=OK' //replace 'default' if active				
			},
		"TX": {
			name: 'Texas',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=TX' //replace 'default' if active				
			},
		"NM": {
			name: 'New Mexico',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=NM' //replace 'default' if active			
			},
		"KS": {
			name: 'Kansas',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=KS' //replace 'default' if active				
			},
		"NE": {
			name: 'Nebraska',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=NE' //replace 'default' if active			
			},
		"SD": {
			name: 'South Dakota',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=SD' //replace 'default' if active	
			},
		"ND": {
			name: 'North Dakota',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=ND' //replace 'default' if active	
			},
		"WY": {
			name: 'Wyoming',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=WY' //replace 'default' if active	
			},
		"MT": {
			name: 'Montana',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=MT' //replace 'default' if active	
			},
		"CO": {
			name: 'Colorado',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=CO' //replace 'default' if active	
			},
		"UT": {
			name: 'Utah',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=UT' //replace 'default' if active	
			},
		"AZ": {
			name: 'Arizona',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=AZ' //replace 'default' if active	
			},
		"NV": {
			name: 'Nevada',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=NV' //replace 'default' if active			
			},
		"OR": {
			name: 'Oregon',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=OR' //replace 'default' if active			
			},
		"WA": {
			name: 'Washington',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no',
			url: 'grantees.aspx?statecode=WA'				
			},
		"CA": {
			name: 'California',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no',
			url: 'grantees.aspx?statecode=CA'					
			},
		"MI": {
			name: 'Michigan',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=MI' //replace 'default' if active				
			},
		"ID": {
			name: 'Idaho',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			hover_color: 'default',
			inactive: 'no', //change to 'yes' is inactive	
			url: 'grantees.aspx?state=ID' //replace 'default' if active	
			},
		// Territories - Hidden unless hide is set to "no"
		"GU": {
			name: 'Guam',
			description: 'default',
			color: 'default',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			},
		"VI": {
			name: 'Virgin Islands',
			image_source: 'x.png',			
			description: 'default',
			color: 'default',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			},
		"PR": {
			name: 'Puerto Rico',
			description: 'default',
			color: 'default',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			},
			//loop
		"AK": {
			name: 'Alaska',
			description: 'default',
			color: '#001C55', //replace 'default' if active
			inactive: 'no', //change to 'yes' is inactive		
			hover_color: 'default',
			url: 'grantees.aspx?statecode=AK' //replace 'default' if active						
			},	
		"MP": {
			name: 'Northern Mariana Islands',
			description: 'default',
			color: 'default',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			}			
		},
	
	locations:{
		"0": { //must give each location an id, so that you can reference it later
			name: "New York",
			lat: 40.71, 
			lng: -74.00,
			description: 'default',
			color: 'default',
			url: 'default',
			type: 'default',
			size: 'default', 
			hide: 'yes'
		},
		1: {
			name: 'Anchorage',
			lat: 61.2180556,
			lng: -149.9002778, 
			color: 'default',
			type: 'circle',
			hide: 'yes'			
		}
	},
	
	labels:{
		"HI": {
			color: 'default',
			hover_color: 'default',
			font_family: 'default',
			pill: 'yes',	
			width: 'default',			
		}
	}
	
}




