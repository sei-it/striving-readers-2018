﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Striving Readers - Home</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" Runat="Server">
                    <section class="head-image">
                    <img src="images/header-image.jpg" class="img-responsive" alt="Group of diverse people" />
                </section>

                <section class="bg-blue center home-section-pad">
                    <div class="container">
                        <h2>Striving Readers Comprehensive Literacy Program</h2>
                    </div>
                </section>
                
                
                <section class="home-features">
                	<div class="container">
						<p>Welcome to The Striving Readers Comprehensive Literacy (SRCL) Community of Practice (CoP).  The CoP supports The U.S. Department of Education’s SRCL program designed to create a comprehensive literacy program to advance literacy skills — including pre-literacy skills, reading, and writing — for students from birth through grade 12, including limited-English-proficient students and students with disabilities. The COP is a technical assistance resource for SRCL grantees and sub grantees and the greater education community to enhance peer-to- peer networking.</p>
						
						<p>The CoP website offers tools, information, and strategies to assist in planning, implementing, and evaluating Striving Readers programs. Browse our site to discover best practices and innovative approaches. Our resources include, strong and moderate evidence-based practices, news about professional learning events, and other information that will be useful for your work.  Check back often for new additions to the CoP.</p>
					</div>
				</section>

                
               <section class="home-features discussion-board">
                    <div class="container">
                        <h2>DISCUSSION BOARD</h2>
                      <div class="text-block col-lg-12 col-xs-12">
                            <div class="col-lg-2 col-sm-4 col-xs-12">
                   	  		  <img class="icon" src="images/discussion-icon.png"  alt="Webinars icon"/>
                        </div>
                            <div class="col-lg-1 hidden-sm hidden-xs"></div>
                            <div class="col-lg-7 col-sm-8 col-xs-12">
								<p>Lorem ipsum dolor sit amet, laoreet dolor nec nulla tempus, pellentesque id, cras suscipit pellentesque turpis neque lobortis luctus, magnis nonummy per qui ut phasellus, varius dui leo semper consectetuer tortor conubia. Luctus id neque sed sit urna accumsan, odio convallis luctus parturient ut risus.</p>
                                <a class="btn-view" href="forum/default.aspx">View All <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                    </div>
                </section>               
                
                
 
               <section class="home-features resources">
                    <div class="container">
                        <h2>RESOURCES</h2>
                        
                        
                      <div class="text-block col-lg-12 col-xs-12">
                            <div class="col-lg-2 col-sm-4 col-xs-12">
               	  			  <img class="icon" src="images/resources-icon.png"  alt="Resources icon"/>
                          </div>
                            <div class="col-lg-1 hidden-sm hidden-xs"></div>
                            <div class="col-lg-7 col-sm-8 col-xs-12">
								<p>Lorem ipsum dolor sit amet, laoreet dolor nec nulla tempus, pellentesque id, cras suscipit pellentesque turpis neque lobortis luctus, magnis nonummy per qui ut phasellus, varius dui leo semper consectetuer tortor conubia. Luctus id neque sed sit urna accumsan, odio convallis luctus parturient ut risus.</p>
                              <a class="btn-view" href="resources.aspx">View All <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                    </div>
                </section>               
                               
                                                             
 
                 <section class="home-features webinars">
    
                    <div class="container">
                        <h2>CoP EVENTS</h2>
                      <div class="text-block col-lg-12 col-xs-12">
                            <div class="col-lg-2 col-sm-4 col-xs-12">
                       	  		<img class="icon" src="images/webinar-icon.png"  alt="Webinars icon"/>
                        </div>
                            <div class="col-lg-1 hidden-sm hidden-xs"></div>
                            <div class="col-lg-7 col-sm-8 col-xs-12">
								<p>Lorem ipsum dolor sit amet, laoreet dolor nec nulla tempus, pellentesque id, cras suscipit pellentesque turpis neque lobortis luctus, magnis nonummy per qui ut phasellus, varius dui leo semper consectetuer tortor conubia. Luctus id neque sed sit urna accumsan, odio convallis luctus parturient ut risus.</p>
                                <a class="btn-view" href="webinars-upcoming.aspx">View All <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                    </div>
                </section>               
                               
                                              
                                                                            
                <section class="home-features techAssist">
    
                    <div class="container">
                        <h2>TECHNICAL ASSISTANCE</h2>
                      <div class="text-block col-lg-12 col-xs-12">
                            <div class="col-lg-2 col-sm-4 col-xs-12">
                       	  		<img class="icon" src="images/ta-icon.png"  alt="Technical Assistance icon"/>
                        </div>
                            <div class="col-lg-1 hidden-sm hidden-xs"></div>
                            <div class="col-lg-7 col-sm-8 col-xs-12">
								<p>Lorem ipsum dolor sit amet, laoreet dolor nec nulla tempus, pellentesque id, cras suscipit pellentesque turpis neque lobortis luctus, magnis nonummy per qui ut phasellus, varius dui leo semper consectetuer tortor conubia. Luctus id neque sed sit urna accumsan, odio convallis luctus parturient ut risus.</p>
                                <a class="btn-view" href="requestForm.aspx">View All <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                    </div>
                </section>                
                
                
                


                
                

                
                

                
            <section class="home-bottom-image">
              <img class="img-responsive" src="images/home-bottom-image.jpg" alt="Image of students reading books and different media devices."/>
               
               </section> 
</asp:Content>

