


using System;
using SubSonic.Schema;
using System.Collections.Generic;
using SubSonic.DataProviders;
using System.Data;

namespace Synergy.StrivingReader {
	
        /// <summary>
        /// Table: Resources
        /// Primary Key: ID
        /// </summary>

        public class ResourcesTable: DatabaseTable {
            
            public ResourcesTable(IDataProvider provider):base("Resources",provider){
                ClassName = "Resource";
                SchemaName = "dbo";
                

                Columns.Add(new DatabaseColumn("ID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("PublicationTitle", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 5000
                });

                Columns.Add(new DatabaseColumn("PublicationType", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("PublicationSubType", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("PublicationTopic", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("PublicationKeyword", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 5000
                });

                Columns.Add(new DatabaseColumn("Organization", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 3000
                });

                Columns.Add(new DatabaseColumn("Description", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("PublicationDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 20
                });

                Columns.Add(new DatabaseColumn("Authors", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 1000
                });

                Columns.Add(new DatabaseColumn("OriginalFileName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 500
                });

                Columns.Add(new DatabaseColumn("PhysicalFileName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 500
                });

                Columns.Add(new DatabaseColumn("CreateDate", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.DateTime,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Display", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn ID{
                get{
                    return this.GetColumn("ID");
                }
            }
				
   			public static string IDColumn{
			      get{
        			return "ID";
      			}
		    }
            
            public IColumn PublicationTitle{
                get{
                    return this.GetColumn("PublicationTitle");
                }
            }
				
   			public static string PublicationTitleColumn{
			      get{
        			return "PublicationTitle";
      			}
		    }
            
            public IColumn PublicationType{
                get{
                    return this.GetColumn("PublicationType");
                }
            }
				
   			public static string PublicationTypeColumn{
			      get{
        			return "PublicationType";
      			}
		    }
            
            public IColumn PublicationSubType{
                get{
                    return this.GetColumn("PublicationSubType");
                }
            }
				
   			public static string PublicationSubTypeColumn{
			      get{
        			return "PublicationSubType";
      			}
		    }
            
            public IColumn PublicationTopic{
                get{
                    return this.GetColumn("PublicationTopic");
                }
            }
				
   			public static string PublicationTopicColumn{
			      get{
        			return "PublicationTopic";
      			}
		    }
            
            public IColumn PublicationKeyword{
                get{
                    return this.GetColumn("PublicationKeyword");
                }
            }
				
   			public static string PublicationKeywordColumn{
			      get{
        			return "PublicationKeyword";
      			}
		    }
            
            public IColumn Organization{
                get{
                    return this.GetColumn("Organization");
                }
            }
				
   			public static string OrganizationColumn{
			      get{
        			return "Organization";
      			}
		    }
            
            public IColumn Description{
                get{
                    return this.GetColumn("Description");
                }
            }
				
   			public static string DescriptionColumn{
			      get{
        			return "Description";
      			}
		    }
            
            public IColumn PublicationDate{
                get{
                    return this.GetColumn("PublicationDate");
                }
            }
				
   			public static string PublicationDateColumn{
			      get{
        			return "PublicationDate";
      			}
		    }
            
            public IColumn Authors{
                get{
                    return this.GetColumn("Authors");
                }
            }
				
   			public static string AuthorsColumn{
			      get{
        			return "Authors";
      			}
		    }
            
            public IColumn OriginalFileName{
                get{
                    return this.GetColumn("OriginalFileName");
                }
            }
				
   			public static string OriginalFileNameColumn{
			      get{
        			return "OriginalFileName";
      			}
		    }
            
            public IColumn PhysicalFileName{
                get{
                    return this.GetColumn("PhysicalFileName");
                }
            }
				
   			public static string PhysicalFileNameColumn{
			      get{
        			return "PhysicalFileName";
      			}
		    }
            
            public IColumn CreateDate{
                get{
                    return this.GetColumn("CreateDate");
                }
            }
				
   			public static string CreateDateColumn{
			      get{
        			return "CreateDate";
      			}
		    }
            
            public IColumn Display{
                get{
                    return this.GetColumn("Display");
                }
            }
				
   			public static string DisplayColumn{
			      get{
        			return "Display";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: ResourceCategory
        /// Primary Key: ID
        /// </summary>

        public class ResourceCategoryTable: DatabaseTable {
            
            public ResourceCategoryTable(IDataProvider provider):base("ResourceCategory",provider){
                ClassName = "ResourceCategory";
                SchemaName = "dbo";
                

                Columns.Add(new DatabaseColumn("ID", this)
                {
	                IsPrimaryKey = true,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("ResourceID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("CategoryID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn ID{
                get{
                    return this.GetColumn("ID");
                }
            }
				
   			public static string IDColumn{
			      get{
        			return "ID";
      			}
		    }
            
            public IColumn ResourceID{
                get{
                    return this.GetColumn("ResourceID");
                }
            }
				
   			public static string ResourceIDColumn{
			      get{
        			return "ResourceID";
      			}
		    }
            
            public IColumn CategoryID{
                get{
                    return this.GetColumn("CategoryID");
                }
            }
				
   			public static string CategoryIDColumn{
			      get{
        			return "CategoryID";
      			}
		    }
            
                    
        }
        
        /// <summary>
        /// Table: grantees
        /// Primary Key: 
        /// </summary>

        public class granteesTable: DatabaseTable {
            
            public granteesTable(IDataProvider provider):base("grantees",provider){
                ClassName = "grantee";
                SchemaName = "dbo";
                

                Columns.Add(new DatabaseColumn("ID", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Int32,
	                IsNullable = false,
	                AutoIncrement = true,
	                IsForeignKey = false,
	                MaxLength = 0
                });

                Columns.Add(new DatabaseColumn("Grantee", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 250
                });

                Columns.Add(new DatabaseColumn("State", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 15
                });

                Columns.Add(new DatabaseColumn("City", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("Abstract", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = -1
                });

                Columns.Add(new DatabaseColumn("AbstractFile", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 200
                });

                Columns.Add(new DatabaseColumn("Cohort", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = false,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("ContactName", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 100
                });

                Columns.Add(new DatabaseColumn("ContactEmail", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("ContactPhone", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.AnsiString,
	                IsNullable = true,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 50
                });

                Columns.Add(new DatabaseColumn("isActive", this)
                {
	                IsPrimaryKey = false,
	                DataType = DbType.Boolean,
	                IsNullable = false,
	                AutoIncrement = false,
	                IsForeignKey = false,
	                MaxLength = 0
                });
                    
                
                
            }

            public IColumn ID{
                get{
                    return this.GetColumn("ID");
                }
            }
				
   			public static string IDColumn{
			      get{
        			return "ID";
      			}
		    }
            
            public IColumn Grantee{
                get{
                    return this.GetColumn("Grantee");
                }
            }
				
   			public static string GranteeColumn{
			      get{
        			return "Grantee";
      			}
		    }
            
            public IColumn State{
                get{
                    return this.GetColumn("State");
                }
            }
				
   			public static string StateColumn{
			      get{
        			return "State";
      			}
		    }
            
            public IColumn City{
                get{
                    return this.GetColumn("City");
                }
            }
				
   			public static string CityColumn{
			      get{
        			return "City";
      			}
		    }
            
            public IColumn Abstract{
                get{
                    return this.GetColumn("Abstract");
                }
            }
				
   			public static string AbstractColumn{
			      get{
        			return "Abstract";
      			}
		    }
            
            public IColumn AbstractFile{
                get{
                    return this.GetColumn("AbstractFile");
                }
            }
				
   			public static string AbstractFileColumn{
			      get{
        			return "AbstractFile";
      			}
		    }
            
            public IColumn Cohort{
                get{
                    return this.GetColumn("Cohort");
                }
            }
				
   			public static string CohortColumn{
			      get{
        			return "Cohort";
      			}
		    }
            
            public IColumn ContactName{
                get{
                    return this.GetColumn("ContactName");
                }
            }
				
   			public static string ContactNameColumn{
			      get{
        			return "ContactName";
      			}
		    }
            
            public IColumn ContactEmail{
                get{
                    return this.GetColumn("ContactEmail");
                }
            }
				
   			public static string ContactEmailColumn{
			      get{
        			return "ContactEmail";
      			}
		    }
            
            public IColumn ContactPhone{
                get{
                    return this.GetColumn("ContactPhone");
                }
            }
				
   			public static string ContactPhoneColumn{
			      get{
        			return "ContactPhone";
      			}
		    }
            
            public IColumn isActive{
                get{
                    return this.GetColumn("isActive");
                }
            }
				
   			public static string isActiveColumn{
			      get{
        			return "isActive";
      			}
		    }
            
                    
        }
        
}