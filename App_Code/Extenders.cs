﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Reflection;

/// <summary>
/// Summary description for Extenders
/// </summary>
public static class Extenders
{
    public static DataTable ToDataTable<T>(this IEnumerable<T> collection, string tableName)
    {
        DataTable tbl = ToDataTable(collection);
        tbl.TableName = tableName;
        return tbl;
    }


    public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
    {
        DataTable dt = new DataTable();
        foreach (object obj in collection)
        {
            Type t = obj.GetType();
            PropertyInfo[] pis = t.GetProperties();
            if (dt.Columns.Count == 0)
            {
                foreach (PropertyInfo pi in pis)
                {
                    Type pt = pi.PropertyType;
                    if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                        pt = Nullable.GetUnderlyingType(pt);
                    dt.Columns.Add(pi.Name, pt);
                }
            }
            DataRow dr = dt.NewRow();
            foreach (PropertyInfo pi in pis)
            {
                object value = pi.GetValue(obj, null);
                if (value != null)
                    dr[pi.Name] = value;
                else
                    dr[pi.Name] = DBNull.Value;
            }
            dt.Rows.Add(dr);
        }
        return dt;
    }

    public static double StandardDeviation(this IEnumerable<double> values)
    {
        double avg = values.Average();
        return Math.Sqrt(values.Average(v => Math.Pow(v - avg, 2)));
    } 
}

