﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="resources.aspx.cs" Inherits="resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Striving Readers - Webinars</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" runat="Server">
    <section class="head-image">
        <img src="images/resources.jpg" class="img-responsive" alt="Resources Computer with Hands" />
    </section>

    <section class="bg-blue center home-section-pad">
        <div class="container">
            <h2>Resources</h2>
        </div>
    </section>

    <section class="internal-page-content">
        <div class="container">
            <div class="col-lg-12 col-xs-12">
				<p>The SRCL CoP is dedicated to providing high-quality, evidence-based resources to help develop, implement, and evaluate innovative comprehensive literacy programs. To browse resources by topic, select a topic area in the list box menu below and click Search. To search by keyword, enter the keywords in the Search Terms box and click Search.  The resources can also be accessed by type, which includes Doing What Works (DWW) Adolescent Literacy, DWW Reading Comprehension, DWW RTI Reading, DWW Writing, IES Practice Guides, Individual Studies, and WWC Intervention Reports . </p>
               

                <div class="table-responsive">
                    <!--BEGIN HEAD OF DISPLAY ELEMENTS-->

                    <!--END HEAD OF DISPLAY ELEMENTS-->


                    <div class="scrollme">
                        <table class="table table-responsive">
                            <tbody>
                                <tr class="bg-blue">
                                    <th scope="col">Title</th>
                                    <th scope="col">Summary</th>
                                    <th scope="col">Link</th>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td><a href="#" class="btn btn-grantee">More Details</a></td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td><a href="#" class="btn btn-grantee">More Details</a></td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td><a href="#" class="btn btn-grantee">More Details</a></td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td><a href="#" class="btn btn-grantee">More Details</a></td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td><a href="#" class="btn btn-grantee">More Details</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

        </div>
    </section>
</asp:Content>

