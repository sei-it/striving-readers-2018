﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webinars_archivesDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string str1 = @"<table>
        <tr style='border-color: White; border-style: None;'>
            <td><a name='event6'></a>
                <h2>AECT International Convention</h2>
                <p><strong>Organization</strong>: Association for Educational Communications and Technology<br />
                    <strong>Date</strong>: October 15-21, 2017<br />
                    <strong>Location</strong>: Las Vegas, NV<br />
                    <strong>Description</strong>: The Association for Educational Communications and Technology (AECT) is a professional association of thousands of educators and others whose activities are directed toward improving instruction through technology.&nbsp; AECT members may be found in colleges and universities; in the Armed Forces and industry; in museums, libraries, and hospitals; in the many places where educational change is underway.&nbsp; AECT members carry out a wide range of responsibilities in the study, planning, application, and production of communications media for instruction. <a target='_blank' href='http://aect.site-ym.com/?futureevents'>Learn more</a><br />
                </p>
            </td>
        </tr>
    </table>";

        string str2 = @"<table>
        <tr style='border-color: White; border-style: None;'>
            <td><a name='event7'></a>
                <h2>NAME 26th Annual Conference</h2>
                <p><strong>Organization</strong>: National Association for Multicultural Education<br />
                    <strong>Date</strong>: October 15-20, 2016<br />
                    <strong>Location</strong>: Cleveland, OH<br />
                    <strong>Description</strong>: Through NAME 2016 we continue to improve our problem-solving abilities related to the existing “isms” that damage our society. This year’s conference aims at sharing practices that enhance the human condition for our global society to effectively face the inequities of, that include but not limited to, forms of violence, inequitable distribution of resources, institutional systems that oppress (consciously or unconsciously), those who are disadvantaged and marginalized. By sharing our work and experiences, we are extending our hands in solidarity with ALL who face these forms of injustices. As pioneers of a world striving for equity for ALL, the conference seeks to embrace every individual as a human being with dignity and respect. <a target='_blank' href='http://nameorg.org/headline_news.php' target='_blank'>Learn more</a></p>
            </td>
        </tr>
    </table>";

        string str3 = @"<table>
        <tr style='border-color: White; border-style: None;'>
            <td><a name='event8'></a>
                <h2>Faces of Advocacy: NCTE Annual Convention</h2>
                <p>
                    <strong>Organization</strong>: National Council of Teachers of English<br />
                    <strong>Date</strong>: October 20-22, 2015<br />
                    <strong>Location</strong>: Atlanta, GA<br />
                    <strong>Description</strong>:
                    <div>When we hear the word &quot;advocacy&quot; in connection to our profession, every one of us—teachers, instructors, professors, researchers, writers—has a different definition of what that means. There are people who might think that this only has to do with talking to senators and representatives in government about affecting changes on a state or national level. While this is certainly a part of advocacy, it is not the only piece of advocacy that this Convention wishes to explore.</div>
                    <div>
                        <br />
                    </div>
                    <div>There are many faces of advocacy to think about and explore. Teachers as advocates for/as: Themselves and other teachers; Our profession; Our own well-being and health; Our students; Literacy and learning; Social change; Researchers and writers; Parents, grandparents, or guardians. <a target='_blank' href='http://www.ncte.org/annual' target='_blank'>Learn more</a></div>
                </p>
            </td>
        </tr>
    </table>";

        string str4 = @"  <table>
        <tr style='border-color: White; border-style: None;'>
            <td><a name='event9'></a>
                <h2>Innovative Leadership: Navigating Changes in Literacy Education</h2>
                <p><strong>Organization</strong>: National Council of Teachers of English<br />
                    <strong>Date</strong>: September 15-20, 2014<br />
                    <strong>Location</strong>: Atlanta, GA<br />
                    <strong>Description</strong>: We all know that change is inevitable. Changes in education, however, can be extremely demanding on teachers, administrators, students and parents alike. How can today’s literacy leaders not only bring about necessary changes but navigate changes imposed upon us? Educational policy, standards, high-stakes &nbsp;testing, college readiness — these constantly evolving and controversial issues create tension, chaos and concern for the future of education. How can innovators — the fools, the madmen — navigate the rough seas of change? How can we steer literacy education in the right direction? How can we fight against injustice and focus on promoting literacy? How can literacy leaders bring about change, build communities and prepare teachers for the road ahead? In what ways can innovative leaders expand literacy capacities on the local, state and national levels? <a target='_blank' href='http://www.ncte.org/cel/convention' target='_blank'>Learn more</a></p>
            </td>
        </tr>
    </table>";
        string str5 = @" <table>
        <tr style='border-color: White; border-style: None;'>
            <td><a name='event10'></a>
                <h2>Leaders to Learn From </h2>
                <p><strong>Organization</strong>: Education Week
                    <br />
                    <strong>Date</strong>: March 31, 2017<br />
                    <strong>Location</strong>: Washington, DC<br />
                    <strong>Description</strong>: Join more than 200 of America’s most influential leaders from every area of K-12 education. Discover how the next class of Leaders To Learn From honorees have discovered and implemented effective solutions to the challenges facing today’s districts. Meet the most innovative leaders in K-12 education. Learn the keys to their success. <a target='_blank' href=' https://www.regonline.com/Register/Checkin.aspx?EventID=1826647'>Learn more</a><br />
                </p>
            </td>
        </tr>
    </table>";

        int id = Convert.ToInt32(Session["WAID"]);

        switch (id)
        {
            case 1:
                lblContents.Text = str1;
                break;
            case 2:
                lblContents.Text = str2;
                break;
            case 3:
                lblContents.Text = str3;
                break;
            case 4:
                lblContents.Text = str4;
                break;
            case 5:
                lblContents.Text = str5;
                break;
        }
    }

    protected void lbtnClose_Click(object sender, EventArgs e)
    {
        Session["WAID"] = null;
        Response.Redirect("webinars-archives.aspx");
    }
}