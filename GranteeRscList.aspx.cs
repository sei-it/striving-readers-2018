﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GranteeRscList : System.Web.UI.Page
{
    Image sortImage = new Image();
    private DataTable dataTable;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (aspnetforum.Utils.User.CurrentUserID == 0)
            return;
        if (!IsPostBack)
        {
            LoadGrantees();
            LoadGridview();
        }
    }

    private void LoadGrantees()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        int granteeId = 0;
        var rcd = db.ForumUsers.SingleOrDefault(x => x.UserID == aspnetforum.Utils.User.CurrentUserID);
        object datasource = new object();

        if (rcd!=null)
        {
            ddlGrantees.Items.Clear();
            granteeId = rcd.GranteeID;
            if(granteeId ==-1)
            {
                datasource = from r in db.grantees where r.isActive select new { r.ID, Grantee=r.Grantee1};
                ddlGrantees.DataSource = datasource;
                ddlGrantees.DataBind();
                ddlGrantees.Items.Insert(0, new ListItem("Select One", ""));
                ddlGrantees.Items.Insert(1, new ListItem("All Grantees", "-1"));
            }
            else
            {
                datasource = from r in db.grantees where r.ID == granteeId && r.isActive select new { r.ID, Grantee = r.Grantee1 };
                ddlGrantees.DataSource = datasource;
                ddlGrantees.DataBind();
                ddlGrantees.Items.Insert(0, new ListItem("Select One", ""));
            }
            
        }
        if(Session["ddlGranteeSelectedValue"] !=null)
        {
            ddlGrantees.SelectedValue = Session["ddlGranteeSelectedValue"].ToString();
        }
       
    }

    private void LoadGridview()
    {
        int granteeId = ddlGrantees.SelectedValue == "" ? 0 : Convert.ToInt32(ddlGrantees.SelectedValue);
        ddlTopics.DataBind();
        int topicId = ddlTopics.SelectedValue == "" ? 0 : Convert.ToInt32(ddlTopics.SelectedValue);

        DataClassesDataContext db = new DataClassesDataContext();
        var rscList1 = new object();
        if (granteeId == -1) //all grantees
        {
            if (ddlTopics.SelectedValue != "")
            {
                var rscList = from rcd in db.vw_granteerscs
                              where rcd.topicid == topicId  
                              select new
                              {
                                  rcd.id,
                                  rcd.Grantee,
                                  rcd.granteeid,
                                  rcd.topicname,
                                  rcd.topicid,
                                  rcd.URL,
                                  rcd.ResourceDes,
                                  rcd.ResourceName,
                                  rcd.OriginalFileName,
                                  rcd.PhysicalFileName,
                                  rcd.isActive,
                                  FilePath = "<a target='_blank' href='docs/GranteeRsc/" + rcd.PhysicalFileName + "'>" + rcd.OriginalFileName + "</a>"
                              };
                gvGranteeRsc.DataSource = rscList;
                gvGranteeRsc.DataBind();
                Session["SRdataTable"] = rscList.ToDataTable();
            }
            else
            {
                var rscList = from rcd in db.vw_granteerscs

                              select new
                              {
                                  ID = rcd.id,
                                  Grantee = rcd.Grantee,
                                  rcd.granteeid,
                                  rcd.topicname,
                                  rcd.topicid,
                                  rcd.URL,
                                  rcd.ResourceDes,
                                  ResourceName = rcd.ResourceName,
                                  rcd.OriginalFileName,
                                  rcd.PhysicalFileName,
                                  FilePath = "<a target='_blank' href='docs/GranteeRsc/" + rcd.PhysicalFileName + "'>" + rcd.OriginalFileName + "</a>",
                                  rcd.isActive
                              };
                gvGranteeRsc.DataSource = rscList;
                gvGranteeRsc.DataBind();
                Session["SRdataTable"] = rscList.ToDataTable();
            }
        }
        else //petical grantee
        {
            if (ddlTopics.SelectedValue != "")
            {
                var rscList = from rcd in db.vw_granteerscs
                              where rcd.granteeid == granteeId && rcd.topicid == topicId

                              select new
                              {
                                  rcd.id,
                                  rcd.Grantee,
                                  rcd.granteeid,
                                  rcd.topicname,
                                  rcd.topicid,
                                  rcd.URL,
                                  rcd.ResourceDes,
                                  rcd.ResourceName,
                                  rcd.OriginalFileName,
                                  rcd.PhysicalFileName,
                                  FilePath = "<a target='_blank' href='docs/GranteeRsc/" + rcd.PhysicalFileName + "'>" + rcd.OriginalFileName + "</a>",
                                  rcd.isActive
                              };
                gvGranteeRsc.DataSource = rscList;
                gvGranteeRsc.DataBind();
                Session["SRdataTable"] = rscList.ToDataTable();
            }
            else
            {
                var rscList = from rcd in db.vw_granteerscs
                              where rcd.granteeid == granteeId
 
                              select new
                              {
                                  ID = rcd.id,
                                  Grantee = rcd.Grantee,
                                  rcd.granteeid,
                                  rcd.topicname,
                                  rcd.topicid,
                                  rcd.URL,
                                  rcd.ResourceDes,
                                  ResourceName = rcd.ResourceName,
                                  rcd.OriginalFileName,
                                  rcd.PhysicalFileName,
                                  FilePath = "<a target='_blank' href='docs/GranteeRsc/" + rcd.PhysicalFileName + "'>" + rcd.OriginalFileName + "</a>",
                                  rcd.isActive
                              };
                gvGranteeRsc.DataSource = rscList;
                gvGranteeRsc.DataBind();
                Session["SRdataTable"] = rscList.ToDataTable();
            }
        }
        
       

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Session["ddlGranteeSelectedValue"] = ddlGrantees.SelectedValue;
        Session["ddlTopicSelectedValue"] = ddlTopics.SelectedValue;
        LoadGridview();
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        Session["srGsrcId"] = null;
        Session["ddlTopicSelectedValue"] = null;
        Session["ddlGranteeSelectedValue"] = null;

        Response.Redirect("granteersc.aspx");
    }
    protected void onEdit(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Session["srGsrcId"] = id;
        Response.Redirect("granteersc.aspx");
    }

    protected void gvGranteeRsc_Sorting(object sender, GridViewSortEventArgs e)
    {

        setSortDirection(SortDireaction);
        dataTable = Session["SRdataTable"] as DataTable;

        if (dataTable != null)
        {
            //Sort the data.
            dataTable.DefaultView.Sort = e.SortExpression + " " + _sortDirection;
            gvGranteeRsc.DataSource = dataTable;
            gvGranteeRsc.DataBind();
            SortDireaction = _sortDirection;
            int columnIndex = 0;
            foreach (DataControlFieldHeaderCell headerCell in gvGranteeRsc.HeaderRow.Cells)
            {
                if (headerCell.ContainingField.SortExpression == e.SortExpression)
                {
                    columnIndex = gvGranteeRsc.HeaderRow.Cells.GetCellIndex(headerCell);
                }
            }

            gvGranteeRsc.HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
        }
    }

    private void setSortDirection(string sortDirection)
    {
        if (sortDirection == "ASC")
        {
            _sortDirection = "DESC";
        }
        else
        {
            _sortDirection = "ASC";
        }
    }
    private string _sortDirection;
    public string SortDireaction
    {
       
        get
        {
            if (ViewState["SortDireaction"] == null)
                return string.Empty;
            else
                return ViewState["SortDireaction"].ToString();
        }
        set
        {
            ViewState["SortDireaction"] = value;
        }
        
    }

    protected void SetSortDirection(string sortDirection)
    {
        if (sortDirection == "ASC")
        {
            _sortDirection = "DESC";
            sortImage.ImageUrl = "view_sort_ascending.png";

        }
        else
        {
            _sortDirection = "ASC";
            sortImage.ImageUrl = "view_sort_descending.png";
        }
    }

    bool firstTime = true;
    protected void gvGranteeRsc_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
            if (this.firstTime)
            {
                System.Data.DataView dv =
                     (e.Row.DataItem as System.Data.DataRowView).DataView;
                this.dataTable = dv.ToTable();
                this.firstTime = false;
            }
    }

    protected void ddlTopics_DataBound(object sender, EventArgs e)
    {
        if (Session["ddlTopicSelectedValue"] != null)
        {
            ddlTopics.SelectedValue = Session["ddlTopicSelectedValue"].ToString();
        }
    }

    
}