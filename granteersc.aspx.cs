﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class granteersc : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (aspnetforum.Utils.User.CurrentUserID == 0)
            return;
        if (!IsPostBack)
        {
            DataClassesDataContext db = new DataClassesDataContext();
            
            int gResId =Session["srGsrcId"]==null?0: Convert.ToInt32(Session["srGsrcId"]);
            GranteeResource grsc  = db.GranteeResources.SingleOrDefault(x => x.id == gResId)==null? new GranteeResource() : db.GranteeResources.SingleOrDefault(x => x.id == gResId);
            LoadData(grsc);
        }
    }

    private void LoadData(GranteeResource grsc)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        int granteeId = grsc.granteeid == null ? 0 : Convert.ToInt32(grsc.granteeid);
        var rcd = db.ForumUsers.SingleOrDefault(x => x.UserID == aspnetforum.Utils.User.CurrentUserID);
        object datasource = new object();

        if (rcd != null)
        {
            ddlGrantees.Items.Clear();
            granteeId = rcd.GranteeID;
            if (granteeId == -1)
            {
                datasource = from r in db.grantees select new { r.ID, Grantee = r.Grantee1 };
                ddlGrantees.DataSource = datasource;
                ddlGrantees.DataBind();
            }
            else
            {
                datasource = from r in db.grantees where r.ID == granteeId select new { r.ID, Grantee = r.Grantee1 };
                ddlGrantees.DataSource = datasource;
                ddlGrantees.DataBind();
            }
            ddlGrantees.Items.Insert(0, new ListItem("Select One", ""));
            ddlGrantees.SelectedValue = granteeId.ToString();
        }
        ddlGrantees.SelectedValue = grsc.granteeid == null ? "" : grsc.granteeid.ToString();
        ddlTopic.SelectedValue = grsc.topicid == null ? "" : grsc.topicid.ToString();
        txtRscName.Text = grsc.ResourceName;
        txtRscDes.Text = grsc.ResourceDes;
        txtURL.Text = grsc.URL;
        lblFileName.Text = "<a target='_blank' href='docs/GranteeRsc/" + grsc.PhysicalFileName + "'>" + grsc.OriginalFileName + "</a>";
        if (aspnetforum.Utils.User.IsAdministrator(aspnetforum.Utils.User.CurrentUserID))
        {
            rwActive.Visible = true;
        }
        else
            rwActive.Visible = false;

        cbxIsActive.Checked = grsc.isActive==null? false:(bool)grsc.isActive;
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("GranteeRscList.aspx");
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();

        GranteeResource oneRecord = new GranteeResource();
        if(Session["srGsrcId"]!=null) //edit mode
        {
            oneRecord = db.GranteeResources.SingleOrDefault(x => x.id == Convert.ToInt32(Session["srGsrcId"]));
        }
        oneRecord.granteeid = Convert.ToInt32(ddlGrantees.SelectedValue);
        oneRecord.topicid = Convert.ToInt32(ddlTopic.SelectedValue);
        oneRecord.URL = txtURL.Text;
        oneRecord.ResourceDes = txtRscDes.Text;
        oneRecord.ResourceName = txtRscName.Text;
        oneRecord.isActive = cbxIsActive.Checked;
        string strPath = aspnetforum.Utils.User.GetUserNameById(aspnetforum.Utils.User.CurrentUserID);
        string CurrentUserName = strPath;
        oneRecord.Modifiedby = CurrentUserName;
        oneRecord.ModifiedDate = DateTime.Now;
        if (FileUpload1.HasFile)
        {
            string strFileroot = Server.MapPath("") + "/Docs/GranteeRsc/";
            string strFileName = FileUpload1.FileName.Replace("#", "");

            string strPathandFile = strPath + "/" + strFileName;

            oneRecord.PhysicalFileName = strPathandFile;
            oneRecord.OriginalFileName = strFileName;

            if (!Directory.Exists(strPath))
            {
                Directory.CreateDirectory(strFileroot + strPath);
            }

            FileUpload1.SaveAs(strFileroot + strPathandFile);
        }
        if (Session["srGsrcId"] == null) //new record
        {
            oneRecord.Createdby = CurrentUserName;
            oneRecord.CreatedDate = DateTime.Now;
            db.GranteeResources.InsertOnSubmit(oneRecord);

        }
        db.SubmitChanges();
        btnClose_Click(sender, e);
    }
}