﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace aspnetforum
{
    public partial class recent_iphone : ForumPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            recentposts.BindRecentPostsRepeater(rptMessagesList, PageSize);
        }
    }
}
