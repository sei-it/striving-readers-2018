﻿<%@ Page Title="" Language="C#" MasterPageFile="AspNetForumMaster.Master" AutoEventWireup="true" CodeFile="ManageUpcomingList.aspx.cs" Inherits="Forum_ManageUpcomingList" %>

<asp:Content ID="UpcomingEventHead" runat="server" ContentPlaceHolderID="ContentPlaceHolderHEAD">
</asp:Content>
<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="UpcomingEventContent" runat="server">
    <section class="home-features">
      <div class="container">
            <div>
                <asp:Button ID="btnNew" runat="server" Text="New" OnClick="btnNew_Click" CssClass="btn btn-grantee" CausesValidation="false" />
            </div>
          <asp:GridView ID="gvUpcommingEvents" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal" AllowSorting="true" 
                        ShowHeaderWhenEmpty="true" OnSorting="gvUpcommingEvents_Sorting"  >
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
        
                        <Columns>
                             <asp:BoundField DataField="Topic" SortExpression="" HeaderText="Topic" />
                             <asp:TemplateField HeaderText="Description">
                                 <ItemTemplate>
                                     <asp:Literal ID="Literal1" runat="server" Text='<%# getDesShort((int)Eval("id"))%>'></asp:Literal>
                                 </ItemTemplate>
                             </asp:TemplateField>
                            <asp:TemplateField HeaderText="Instructor/Date">
                                 <ItemTemplate>
                                     <asp:Literal ID="Literal2" runat="server" Text='<%# getInstructor_Date((int)Eval("id"))%>'></asp:Literal>
                                 </ItemTemplate>
                             </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="onEdit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
            
      </div>
     </section>   
</asp:Content>

