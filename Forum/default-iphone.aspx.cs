﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;

namespace aspnetforum
{
    public partial class default_iphone : ForumPage
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
			DataSet ds = Utils.Forum.GetForumsForFrontpage(CurrentUserID);

            this.rptGroupsList.DataSource = ds.Tables[0];
            this.rptGroupsList.DataBind();

            rptGroupsList.Visible = (rptGroupsList.Items.Count != 0);
        }

        protected void rptGroupsList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater nestedRepeater = e.Item.FindControl("rptForumsList") as Repeater;
                DataRowView drv = e.Item.DataItem as DataRowView;
                DataView dv = drv.CreateChildView("ForumGroupsForums");
                if (dv.Count == 0)
                {
                    e.Item.Visible = false;
                }
                else
                {
                    nestedRepeater.DataSource = dv;
                    nestedRepeater.DataBind();
                }
            }
        }
    }
}
