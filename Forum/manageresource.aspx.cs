﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;

public partial class admin_manageresource : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        Resource item = db.Resources.SingleOrDefault(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        if (item != null)
        {
            db.Resources.DeleteOnSubmit(item);
            db.SubmitChanges();
        }
        
        //MagnetPublication.Delete(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        LoadData(GridView1.PageIndex);
    }
    protected void OnGridViewPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var data = (from m in db.Resources
                    orderby m.CreateDate descending
                    select new { m.ID, m.PublicationTitle, m.PublicationType, m.Organization, m.PublicationDate, m.PublicationKeyword }).ToList();
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
    private void ClearFields()
    {
        txtPublicationName.Text = "";
        ddlResourceType.SelectedIndex = 0;
        txtKeywords.Text = "";
        txtDescription.Content = "";
        txtOrganization.Text = "";
        txtPublicationDate.Text = "";
        ddlSubType.SelectedIndex = 0;
        foreach (ListItem li in ddlTopics.Items)
        {
                li.Selected = false;
        }
        hfID.Value = "";
    }
    protected void OnAddPublication(object sender, EventArgs e)
    {
        ClearFields();
        FileUpload1.Enabled = true;
        mpeResourceWindow.Show();
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        ClearFields();
        hfID.Value = (sender as LinkButton).CommandArgument;
        Resource publication = db.Resources.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        txtPublicationName.Text = publication.PublicationTitle;
        ddlResourceType.SelectedValue = publication.PublicationType;
        txtKeywords.Text = publication.PublicationKeyword;
        txtOrganization.Text = publication.Organization;
        txtPublicationDate.Text = publication.PublicationDate;
        txtDescription.Content = publication.Description;
        if (publication.PublicationSubType != null) ddlSubType.SelectedValue = publication.PublicationSubType.ToString();
        //if (publication.PublicationTopic != null) ddlTopics.SelectedValue = publication.PublicationTopic.ToString();
        FileUpload1.Enabled = false;

        var Categories = from rcd in db.ResourceCategories
                         where rcd.ResourceID == Convert.ToInt32(hfID.Value)
                         select rcd;
        foreach (ResourceCategory category in Categories)
        {
            foreach (ListItem li in ddlTopics.Items)
            {
                if (category.CategoryID == Convert.ToInt32(li.Value))
                    li.Selected = true;
            }
        }

        mpeResourceWindow.Show();
    }
    protected void OnSavePublication(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        Resource publication = new Resource();
  
        if (!string.IsNullOrEmpty(hfID.Value))
        {
            publication = db.Resources.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }
        else
        {
            publication = new Resource();
            publication.CreateDate = DateTime.Now;
        }
        publication.PublicationTitle = txtPublicationName.Text;
        publication.PublicationType = ddlResourceType.SelectedValue;
        publication.PublicationKeyword = txtKeywords.Text;
        publication.Organization = txtOrganization.Text;
        publication.PublicationDate = txtPublicationDate.Text;
        publication.Description = txtDescription.Content;
        if (ddlSubType.SelectedIndex > 0) publication.PublicationSubType = Convert.ToInt32(ddlSubType.SelectedValue);
        if (ddlTopics.SelectedIndex > 0) publication.PublicationTopic = Convert.ToInt32(ddlTopics.SelectedValue);
        if (FileUpload1.HasFile)
        {
            string baseFolder = Server.MapPath("../Docs/Resources/");
            string filePrefix = Guid.NewGuid().ToString();
            FileUpload1.SaveAs(baseFolder + filePrefix + FileUpload1.FileName);
            publication.OriginalFileName = FileUpload1.FileName;
            publication.PhysicalFileName = filePrefix + FileUpload1.FileName;
        }
        string CurrentUserName = aspnetforum.Utils.User.GetUserNameById(aspnetforum.Utils.User.CurrentUserID);
        publication.Modifiedby = CurrentUserName;
        publication.ModifiedDate = DateTime.Now;
        if (string.IsNullOrEmpty(hfID.Value))
        {
            publication.Modifiedby = CurrentUserName;
            publication.ModifiedDate = DateTime.Now;
            db.Resources.InsertOnSubmit(publication);
        }
            
        db.SubmitChanges();

        //topics
        foreach (ListItem li in ddlTopics.Items)
        {
            if (li.Selected)
            {
                var item = from rcd in db.ResourceCategories
                           where rcd.ResourceID == publication.ID && rcd.CategoryID == Convert.ToInt32(li.Value)
                           select rcd;
                if (item.Count() == 0)
                {
                    ResourceCategory category = new ResourceCategory();
                    category.ResourceID = publication.ID;
                    category.CategoryID = Convert.ToInt32(li.Value);
                    db.ResourceCategories.InsertOnSubmit(category);
                    db.SubmitChanges();
                }
            }
            else
            {
                var data = from cates in db.ResourceCategories
                           where cates.ResourceID == publication.ID && cates.CategoryID == Convert.ToInt32(li.Value)
                           select cates;
                if (data.Count()>0)
                {
                    var selectedItem = from deleteOne in db.ResourceCategories
                                       where deleteOne.ID == data.First().ID
                                       select deleteOne;

                    //MagnetResourceCategory.Delete(x => x.ID == data[0].ID);
                    db.ResourceCategories.DeleteOnSubmit(selectedItem.First());
                    db.SubmitChanges();
                }
            }
        }

        LoadData(GridView1.PageIndex);
    }
    protected void btnNewTopic_Click(object sender, EventArgs e)
    {
        mpeTopicWindow.Show();
    }

    protected void OnSaveTopic(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        subtopic newTopic = new subtopic();
        newTopic.topicname = txtNewTopic.Text.Trim();
        newTopic.des = txtNewTopicDes.Text.Trim();
        db.subtopics.InsertOnSubmit(newTopic);
        db.SubmitChanges();

        LoadData(GridView1.PageIndex); 
    }
}