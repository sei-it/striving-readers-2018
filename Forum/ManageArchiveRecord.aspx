﻿<%@ Page Title="" Language="C#" MasterPageFile="AspNetForumMaster.Master" AutoEventWireup="true" CodeFile="ManageArchiveRecord.aspx.cs" Inherits="Forum_ManageArchiveRecord" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <section class="home-features">
      <div class="container">
    <h1>Grantee Resource</h1>
    <table>
        <tr>
            <th>
               Event:
            </th>
            <td>
                <asp:TextBox ID="txtEvent" runat="server" Width="680"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEvent" InitialValue="" Display="Dynamic" ForeColor="Red" ErrorMessage="This field is required."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <th>
                Event Start Date:
            </th>
            <td>
                <asp:TextBox ID="txtStartDate" runat="server" Width="680"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStartDate" InitialValue="" Display="Dynamic" ForeColor="Red" ErrorMessage="This field is required."></asp:RequiredFieldValidator>              
                <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtStartDate" Format="yyyy" runat="server"></ajax:CalendarExtender>
            </td>
        </tr>
        <tr>
            <th>
                Event End Date:
            </th>
            <td>
                <asp:TextBox ID="txtEndDate" runat="server" Width="680"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStartDate" InitialValue="" Display="Dynamic" ForeColor="Red" ErrorMessage="This field is required."></asp:RequiredFieldValidator>              
                <ajax:CalendarExtender ID="CalendarExtender2" TargetControlID="txtEndDate" Format="yyyy" runat="server"></ajax:CalendarExtender>
            </td>
        </tr>
         <tr>
            <th>
                Organization:
            </th>
            <td>
                <asp:TextBox ID="txtOrganization" runat="server" Width="680"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <th>
                Location:
            </th>
            <td>
                <asp:TextBox ID="txtLocation" runat="server" Width="680"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <th>
                Details:
            </th>
            <td>
                 <cc1:Editor id="txtDescription" runat="server" width="470" height="200"></cc1:Editor>
            </td>
        </tr>
        <tr>
            <th>URL:</th>
            <td>
                 <asp:TextBox ID="txtURL" runat="server" Width="680"></asp:TextBox>
            </td>
        </tr>
        <tr id="rwActive" runat="server">
            <th>Active:</th>
            <td>
                <asp:CheckBox ID="cbxIsActive" runat="server"  /></td>
        </tr>
    </table>
    <p >
        <asp:Button ID="Submit" runat="server" CssClass="btn btn-grantee" Text="Submit" OnClick="Submit_Click"/>
        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-grantee" Text="Close" CausesValidation="false" OnClick="btnClose_Click" />
    </p>
    <asp:HiddenField ID="hfID" runat="server" />
        </div>
    </section>
</asp:Content>

