﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forum_ManageGrantees : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        LoadData(0);
    }

    protected void EditGrantee(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        hfID.Value = id.ToString();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            grantee rcd = db.grantees.SingleOrDefault(x => x.ID == id);
            txtgranteename.Text = rcd.Grantee1;
            txtDescription.Content = rcd.Abstract;
            txtContactName.Text = rcd.ContactName;
            txtEmail.Text = rcd.ContactEmail;
            cbxActive.Checked = rcd.isActive;
            lblFileName.Text = rcd.AbstractFile;
            ddlState.SelectedValue = rcd.State.Trim();

        }

        mpextGrantee.Show();
    }

    protected void DeleteGrantee(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        grantee item = db.grantees.SingleOrDefault(x => x.ID == Convert.ToInt32((sender as LinkButton).CommandArgument));
        if (item != null)
        {
            db.grantees.DeleteOnSubmit(item);
            db.SubmitChanges();
        }

        LoadData(GridView1.PageIndex);

    }

    protected void OnSave(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        grantee newGrantee = new grantee();

        if (!string.IsNullOrEmpty(hfID.Value))
        {
            newGrantee = db.grantees.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
        }

        newGrantee.Grantee1 = txtgranteename.Text.Trim();
        newGrantee.State = ddlState.SelectedValue;
        newGrantee.City = txtCity.Text.Trim();
        newGrantee.Abstract = txtDescription.Content;
        newGrantee.ContactName = txtContactName.Text.Trim();
        newGrantee.ContactEmail = txtEmail.Text.Trim();
        newGrantee.isActive = cbxActive.Checked;
        string CurrentUserName = aspnetforum.Utils.User.GetUserNameById(aspnetforum.Utils.User.CurrentUserID);
        newGrantee.Modifiedby = CurrentUserName;
        newGrantee.ModifiedDate = DateTime.Now;
        if (FileUpload1.HasFile)
        {
            string baseFolder = Server.MapPath("../Docs/Grantees/");
            string filePrefix = Guid.NewGuid().ToString();
            FileUpload1.SaveAs(baseFolder + FileUpload1.FileName);
            newGrantee.AbstractFile = FileUpload1.FileName;
            //newGrantee.PhysicalFileName = filePrefix + FileUpload1.FileName;
        }
        if (string.IsNullOrEmpty(hfID.Value))
        {
            newGrantee.Createdby = CurrentUserName;
            newGrantee.ModifiedDate = DateTime.Now;
            db.grantees.InsertOnSubmit(newGrantee);
        }
            

        db.SubmitChanges();

        LoadData(GridView1.PageIndex);
    }
    protected void OnAdd(object sender, EventArgs e)
    {
        ClearFields();
        FileUpload1.Enabled = true;
        mpextGrantee.Show();
    }

    private void ClearFields()
    {
        hfID.Value = "";
        txtgranteename.Text = "";
        txtCity.Text = "";
        ddlState.SelectedIndex = 0;
        txtContactName.Text = "";
        txtDescription.Content = "";
        txtEmail.Text = "";
        lblFileName.Text = "";
        cbxActive.Checked = false;
    }

    protected void OnGridViewPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    private void LoadData(int PageNumber)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var data = (from m in db.grantees
                    //where m.isActive
                    orderby m.Grantee1 
                    select new { m.ID, m.Grantee1, m.Abstract, m.State, m.City, m.isActive }).ToList();
        GridView1.DataSource = data;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }

}