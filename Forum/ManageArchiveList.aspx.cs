﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forum_ManageArchiveList : System.Web.UI.Page
{
    private DataTable dataTable;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loadData();
        }
    }

    private void loadData()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var data = from r in db.ArchivedEvents
                   where r.isActive == true
                   select r;
        gvArchiveEvent.DataSource = data;
        gvArchiveEvent.DataBind();

    }

    protected void ShowDetails(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Session["WAID"] = id;
        Response.Redirect("webinars-archivesDetail.aspx");
    }

    protected string getDesShort(int id)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        string shortDes = db.ArchivedEvents.SingleOrDefault(x => x.id == id && x.isActive == true).EventDetails;

        if (string.IsNullOrEmpty(shortDes))
            return "";
        else
        {
            return shortDes.Length >= 250 ? shortDes.Substring(0, 250) + "..." : shortDes;
        }
    }
    protected string getEventDate(int id)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var eventDate = db.ArchivedEvents.SingleOrDefault(x => x.id == id && x.isActive == true);

        if (eventDate == null)
            return "";
        else
        {
            string eventDateRange = "";
            string startDate = eventDate.EventStartDate.Value.ToString("MMMM dd, yyyy");
            string startDay = eventDate.EventStartDate == null ? "" : eventDate.EventStartDate.Value.ToString("dd");
            string endDay = eventDate.EventEndDate == null ? "" : eventDate.EventEndDate.Value.ToString("dd");
            eventDateRange = startDay + (endDay == "" ? "," : " - " + endDay + ",");
            startDay = startDay + ",";
            startDate = startDate.Replace(startDay, eventDateRange);
            return startDate;
        }
    }

    protected void gvArchiveEvent_Sorting(object sender, GridViewSortEventArgs e)
    {

        setSortDirection(SortDireaction);
        dataTable = Session["SRdataTable"] as DataTable;

        if (dataTable != null)
        {
            //Sort the data.
            dataTable.DefaultView.Sort = e.SortExpression + " " + _sortDirection;
            gvArchiveEvent.DataSource = dataTable;
            gvArchiveEvent.DataBind();
            SortDireaction = _sortDirection;
            int columnIndex = 0;
            foreach (DataControlFieldHeaderCell headerCell in gvArchiveEvent.HeaderRow.Cells)
            {
                if (headerCell.ContainingField.SortExpression == e.SortExpression)
                {
                    columnIndex = gvArchiveEvent.HeaderRow.Cells.GetCellIndex(headerCell);
                }
            }

            //gvArchiveEvent.HeaderRow.Cells[columnIndex].Controls.Add(sortImage);
        }
    }

    private void setSortDirection(string sortDirection)
    {
        if (sortDirection == "ASC")
        {
            _sortDirection = "DESC";
        }
        else
        {
            _sortDirection = "ASC";
        }
    }
    private string _sortDirection;
    public string SortDireaction
    {

        get
        {
            if (ViewState["SortDireaction"] == null)
                return string.Empty;
            else
                return ViewState["SortDireaction"].ToString();
        }
        set
        {
            ViewState["SortDireaction"] = value;
        }

    }

    protected void SetSortDirection(string sortDirection)
    {
        if (sortDirection == "ASC")
        {
            _sortDirection = "DESC";
            //sortImage.ImageUrl = "view_sort_ascending.png";

        }
        else
        {
            _sortDirection = "ASC";
            //sortImage.ImageUrl = "view_sort_descending.png";
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        Session["srArchiveId"] = null;

        Response.Redirect("ManageArchiveRecord.aspx");
    }

    protected void onEdit(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Session["srArchiveId"] = id;
        Response.Redirect("ManageArchiveRecord.aspx");
    }
}