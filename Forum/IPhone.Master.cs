﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace aspnetforum
{
    public partial class IPhone : System.Web.UI.MasterPage
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (lblVersion != null)
            {
                string dllversion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                lblVersion.Text = dllversion;
            }

            if (Page is aspnetforum.ForumPage)
            {
                if (((aspnetforum.ForumPage)Page).CurrentUserID == 0) //not logged in
                {
                    if (divLogin != null) divLogin.Visible = true;
                    if (divCurUser != null) divCurUser.Visible = false;
                    if (viewProfileLink != null) viewProfileLink.Visible = false;
                }
                else //logged in
                {
                    if (divLogin != null) divLogin.Visible = false;
                    if (divCurUser != null) divCurUser.Visible = true;
                    if (viewProfileLink != null)
                    {
                        string username = Session["aspnetforumUserName"].ToString();
                        viewProfileLink.InnerHtml = username;
                    }
                }
            }
        }
    }
}
