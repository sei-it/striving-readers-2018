﻿<%@ Page Title="" Language="C#" MasterPageFile="AspNetForumMaster.Master" AutoEventWireup="true" CodeFile="ManageArchiveList.aspx.cs" Inherits="Forum_ManageArchiveList" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" Runat="Server">
    <section class="home-features" >
       <div>
            <asp:Button ID="btnNew" runat="server" Text="New" OnClick="btnNew_Click" CssClass="btn btn-grantee" CausesValidation="false" />
        </div>
      <div class="container">
          <asp:GridView ID="gvArchiveEvent" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal" AllowSorting="true" 
                        ShowHeaderWhenEmpty="true"  >
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
        
                        <Columns>
                            <asp:BoundField DataField="EventName" SortExpression="" HeaderText="Event" />
                             <asp:TemplateField HeaderText="Details" SortExpression="">
                                <ItemTemplate>
                                    <asp:Label ID="lblEvent" runat="server" Text='<%# getDesShort((int)Eval("id"))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" SortExpression="">
                                <ItemTemplate>
                                    <asp:Label ID="lblResourceName" runat="server" Text='<%# getEventDate((int)Eval("id"))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Active" runat="server" SortExpression="isActive">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%#Eval("isActive")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="onEdit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
      </div>
     </section>   
</asp:Content>