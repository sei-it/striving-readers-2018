﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Forum/AspNetForumMaster.Master" AutoEventWireup="true" CodeFile="ManageGrantees.aspx.cs" Inherits="Forum_ManageGrantees" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" Runat="Server">
    <title>Striving Readers - Manage Grantees</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="AspNetForumContentPlaceHolder" Runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    

                

				<section class="internal-page-content">
					<div class="container">
					 <div class="col-lg-12 col-xs-12">
					       <h2>Manage Grantee</h2>
					       
					       <div class="scrollme">
					        <!--BEGIN HEAD OF DISPLAY ELEMENTS-->
					        <!--END HEAD OF DISPLAY ELEMENTS-->
                             <asp:Literal ID="ltlGranteesPlaceHolder" runat="server"></asp:Literal><br />
                            <asp:Button ID="Newbutton" runat="server" Text="New Grantee" CssClass="msapBtn"  OnClick="OnAdd" />
                             <asp:GridView ID="GridView1" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AllowPaging="true" AllowSorting="false"
                        PageSize="20" AutoGenerateColumns="false" OnPageIndexChanging="OnGridViewPageIndexChanged"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal"
                        ShowHeaderWhenEmpty="true">
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
                        <Columns>
                            <asp:TemplateField HeaderText="Grantee">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("Grantee1") %>' Style="font-weight: bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField DataField="PublicationTitle" SortExpression="" HeaderText="Title" HeaderStyle-Font-Bold="true" />--%>
                            <asp:BoundField DataField="Abstract" SortExpression="" HeaderText="Dewscription" />
                            <asp:TemplateField HeaderText="Approved">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbxIsactive" Enabled="false" Checked='<%#Bind("isActive") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="EditGrantee"></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="DeleteGrantee" OnClientClick="return confirm('Are you certain you want to delete this record?');"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                             <asp:HiddenField ID="hfID" runat="server" />
				          </div>
				      </div>
					  </div>
				</section>	
     <asp:Panel ID="pnlSRgrantee" runat="server">
    <div class="mpeDiv">
            <div class="mpeDivHeader">
                Grantee Details
                <span class="closeit"><asp:LinkButton ID="lbtnClose" runat="server" Text="Close" CssClass="closeit_link" />
                </span>
            </div>
            <div class="popContent">
                <table >
                    <tr>
						<th >Grantee Name:</th>
                        <td>
                            <asp:TextBox ID="txtgranteename" runat="server" MaxLength="5000" Width="470" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr >
						<th >
                            Grantee City:
						</th>
						<td>
                            <asp:TextBox ID="txtCity" runat="server" MaxLength="5000" Width="470" ></asp:TextBox>
						</td>
					</tr>
					<tr >
						<th >
                            Grantee State:
						</th>
						<td>
                            <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="True" DataSourceID="dsStates" DataTextField="StateFullName" DataValueField="StateFullName" >
                                <asp:ListItem Value="">Select one</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsStates" runat="server" ConnectionString="<%$ ConnectionStrings:StrivingReaderConnectionString %>" SelectCommand="SELECT [StateFullName], [StateAbb] FROM [States]"></asp:SqlDataSource>
                            <%--<asp:RequiredFieldValidator ID="rfvState"  runat="server" ControlToValidate="dsGrantees" InitialValue="" ErrorMessage="A state is required." />--%>

						</td>
					</tr>
                    <tr>
						<th>
                            Grantee Description:
						</th>
						<td>
                             <cc1:Editor id="txtDescription" runat="server" width="470" height="200">
                                </cc1:Editor>
						</td>
					</tr>
                    <tr>
						<th>
                            Contact Name:
						</th>
						<td>
                            <asp:TextBox ID="txtContactName" runat="server" MaxLength="5000" Width="470" ></asp:TextBox>
						</td>
					</tr>
                    <tr>
                        <th>Contact Email: </th>
                        <td>
                             <asp:TextBox ID="txtEmail" runat="server" MaxLength="5000" Width="470" ></asp:TextBox>
                        </td>
                     </tr>
                    <tr>
                    <th>
                        Document:
                    </th>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <asp:Label ID="lblFileName" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                    <tr>
						<th>
                            Approved:
						</th>
						<td>
                            <asp:CheckBox ID="cbxActive" runat="server" />
						</td>
					</tr>
                     <tr>
                         <td>
                        <asp:Button ID="Button14" runat="server" CssClass="msapBtn" Text="Save Record" OnClick="OnSave" />
                    </td>
                    <td>
                        <asp:Button ID="btnGrntClose" runat="server" CssClass="msapBtn" Text="Close Window" />
                    </td>
                     </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton8" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="mpextGrantee" runat="server" TargetControlID="LinkButton8"
        PopupControlID="pnlSRgrantee" DropShadow="true" OkControlID="btnGrntClose" CancelControlID="btnGrntClose"
        BackgroundCssClass="magnetMPE" Y="20" />

</asp:Content>

