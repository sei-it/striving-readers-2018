﻿<%@ Page Title="" Language="C#" MasterPageFile="AspNetForumMaster.Master" AutoEventWireup="true" CodeFile="ManageUpcomingRecord.aspx.cs" Inherits="Forum_ManageUpcomingRecord" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <section class="home-features">
      <div class="container">
    <h1>UpComing Event</h1>
    <table>
        
        <tr>
            <th>
                Topic:
            </th>
            <td>
                 <asp:TextBox ID="txtTopic" runat="server" Width="680"></asp:TextBox>               
            </td>
        </tr>
        <tr>
            <th>
                Instructor:
            </th>
            <td>
                <asp:TextBox ID="txtInstructor" runat="server" Width="680"></asp:TextBox>          
            </td>
        </tr>
        <tr>
            <th>
                Event Date:
            </th>
            <td>
                <asp:TextBox ID="txtDate" runat="server" Width="680"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDate" Format="yyyy" runat="server"></asp:CalendarExtender>
            </td>
                
        </tr>
        <tr>
             <th>
                Description:
            </th>
             <td>
                <cc1:Editor id="txtDescription" runat="server" width="470" height="200"></cc1:Editor>
            </td>
        </tr>
        <tr runat="server" visible="false">
            <td>
                Uplaod document:
            </td>
             <td>
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="btn btn-grantee" />&nbsp;&nbsp;
                 <asp:Label ID="lblFileName" runat="server" Text="Label"></asp:Label>
                </td>
        </tr>
        <tr>
            <th>URL:</th>
            <td>
                 <asp:TextBox ID="txtURL" runat="server" Width="680"></asp:TextBox>
            </td>
        </tr>
        <tr id="rwActive" runat="server">
            <th>Approved:</th>
            <td>
                <asp:CheckBox ID="cbxIsActive" runat="server"  /></td>
        </tr>
    </table>
    <p >
        <asp:Button ID="Submit" runat="server" CssClass="btn btn-grantee" Text="Submit" OnClick="Submit_Click"/>
        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-grantee" Text="Close" CausesValidation="false" OnClick="btnClose_Click" />
    </p>
    <asp:HiddenField ID="hfID" runat="server" />
        </div>
    </section>
</asp:Content>
