﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using System.Data;
using System.Web.UI.HtmlControls;
using aspnetforum.Utils;

namespace aspnetforum
{
	public partial class messages_iphone : ForumPage
	{
		public int _topicID;
		protected int _forumID;
		protected bool _isModerator = false;
		protected string pagerString = "";
		bool _sortDesc;
		bool _allowGuestPosts;
		bool _bTopicClosed;
		bool _bMembersOnly = false;
		string _forumTitle = "";
		bool _premoderated;

		private void GetGeneralSettings()
		{
			try
			{
				_allowGuestPosts = Utils.Settings.AllowGuestPosts;
				_sortDesc = Utils.Settings.MsgSortDescending;
			}
			catch
			{
				_allowGuestPosts = false;
				_sortDesc = false;
			}
		}

		private bool GetTopicID()
		{
			int.TryParse(Request.QueryString["TopicID"], out _topicID);
			if (_topicID == 0)
			{
				Response.Write("topic not found");
				Response.TrySkipIisCustomErrors = true;
				Response.StatusCode = 404;
				Response.End();
				return false;
			}
			return true;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!GetTopicID()) return;

			GetGeneralSettings();

			Cn.Open();

			GetGeneralTopicInfo();

			//hide "add post" link if user is guest or topic readonly
			ulQuickReply.Visible = (CurrentUserID != 0 || _allowGuestPosts) && !_bTopicClosed;

			if (_bMembersOnly && CurrentUserID == 0)
			{
				Cn.Close();
				Response.Write("The forum <b>\"" + _forumTitle + "\"</b> is for authenticated users only. Please login or register.");
				Response.End();
				return;
			}

			if (!Utils.Forum.CheckForumReadPermissions(_forumID, CurrentUserID))
			{
				Cn.Close();
				Response.Write("Access denied. No permission.");
				Response.End();
				return;
			}

			messages.IncreaseViewsCounter(Cn, _topicID);

			//is it a poll?
			//ShowPollIfAny();

			//bind repeater
			messages.BindMessagesRepeater(_topicID, Cn, rptMessagesList, _isModerator, _sortDesc, Request, PageSize, out pagerString);
			Cn.Close();
		}

		public void GetGeneralTopicInfo()
		{
			//Cn.Open(); the connection should be already open!!!!!!

			DbDataReader dr = Cn.ExecuteReader("SELECT Forums.Title, Forums.ForumID, ForumTopics.Subject, Forums.Premoderated, ForumTopics.IsClosed, Forums.MembersOnly FROM Forums INNER JOIN ForumTopics ON Forums.ForumID=ForumTopics.ForumID WHERE ForumTopics.TopicID=" + _topicID);
			if (dr.Read())
			{
				_forumTitle = dr["Title"].ToString();
				lblCurTopic.Text = dr["Subject"].ToString();
				Title = dr["Subject"] + " - " + dr["Title"];
				MetaDescription = Title;
				MetaKeywords = Title;
				_forumID = Convert.ToInt32(dr["ForumID"]);
				_premoderated = Convert.ToBoolean(dr["Premoderated"]);

				//is closed
				_bTopicClosed = Convert.ToBoolean(dr["IsClosed"]);

				_bMembersOnly = Convert.ToBoolean(dr["MembersOnly"]);
			}
			dr.Close();

			lblCurForum.Text = Utils.Forum.GetForumBreadCrumbs(_forumID, Cn);

			_isModerator = IsModerator(_forumID);

			//Cn.Close(); the connection should be already open!!!!!!
		}

		protected void btnQuickReply_Click(object sender, EventArgs e)
		{
			string msg = tbQuickReply.Text.Trim();
			if (msg == "") return;
			msg = msg.Replace("<", "&lt;").Replace(">", "&gt;");

			Cn.Open();
			int messageId = Utils.Message.AddMessage(Cn, _topicID, msg, !_premoderated || _isModerator, Utils.Various.GetUserIpAddress(Request), false);

			if (_premoderated && !_isModerator)
			{
				Cn.Close();
				Response.Redirect("premoderatedmessage.aspx");
			}
			else
			{
				//count messages to compute the number of pages
				//(needed to get the user redirected to the last page)
				string url = Utils.Topic.GetNewlyPostedMessageUrl(_topicID, messageId, Cn, PageSize);
				Cn.Close();
				Response.Redirect(url);
			}
		}

		protected void rptMessagesList_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			//delete message
			if (e.CommandName == "delete")
			{
				int deletedMessageId = int.Parse(e.CommandArgument.ToString());
				Cn.Open();
				bool topicDeleted = Utils.Message.DeleteMessage(deletedMessageId, Cn);
				if (topicDeleted) //no messages left in the topic
				{
					Cn.Close();
					Response.Redirect(Utils.Various.GetForumURL(_forumID, _forumTitle));
					return;
				}
				messages.BindMessagesRepeater(_topicID, Cn, rptMessagesList, _isModerator, _sortDesc, Request, PageSize, out pagerString);
				Cn.Close();
			}
			//delete message
			if (e.CommandName == "complain")
			{
				int reportMessageId = int.Parse(e.CommandArgument.ToString());
				Cn.Open();
				Utils.Message.ReportToModerator(reportMessageId, CurrentUserID, "", Cn);
				messages.BindMessagesRepeater(_topicID, Cn, rptMessagesList, _isModerator, _sortDesc, Request, PageSize, out pagerString);
				Cn.Close();
				ClientScript.RegisterStartupScript(GetType(), "reported", "<script type='text/javascript'>alert('reported');</script>");
			}
			//approve message (for premoderated forum)
			if (e.CommandName == "approve")
			{
				int approvedMessageId = int.Parse(e.CommandArgument.ToString());

				Cn.Open();
				Utils.Message.ApproveMessage(approvedMessageId, Cn);
				messages.BindMessagesRepeater(_topicID, Cn, rptMessagesList, _isModerator, _sortDesc, Request, PageSize, out pagerString);
				Cn.Close();
			}
		}

		protected void rptMessagesList_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView record = (DataRowView)e.Item.DataItem;

				//allow quoting
				if (_allowGuestPosts || CurrentUserID != 0)
				{
					HtmlAnchor lnkQuote = (HtmlAnchor)e.Item.FindControl("lnkQuote");
					lnkQuote.Visible = !_bTopicClosed; //hide "reply with quote" if topic closed
					lnkQuote.HRef = "addpost.aspx?TopicID=" + record["TopicID"] + "&Quote=" + record["MessageID"];

					LinkButton btnComplain = (LinkButton)e.Item.FindControl("btnComplain");
					btnComplain.Visible = true;
				}

				// Moderators and message owners can delete and edit messages.
				if (CurrentUserID != 0 && (_isModerator || Convert.ToInt32(record["UserID"]) == CurrentUserID))
				{
					//show "delete" button for moderators
					e.Item.FindControl("btnModeratorDelete").Visible = true;

					// Show "edit" button for moderators.
					HtmlAnchor lnkEdit = (HtmlAnchor)e.Item.FindControl("lnkEdit");
					lnkEdit.Visible = true;
					lnkEdit.HRef = "addpost.aspx?TopicID=" + record["TopicID"] + "&Edit=" + record["MessageID"];
				}
				else
				{
					// Need to explicitly turn off this special case control.
					e.Item.FindControl("lnkEdit").Visible = false;
				}

				if (_isModerator)
				{
					if (!Convert.ToBoolean(record["Visible"]))
					{
						e.Item.FindControl("btnModeratorApprove").Visible = true;
					}
				}

				//show attachments
				/*Repeater nestedRepeater = e.Item.FindControl("rptFiles") as Repeater;
				DataRowView dv = e.Item.DataItem as DataRowView;
				nestedRepeater.DataSource = dv.CreateChildView("MessagesFiles");
				nestedRepeater.DataBind();
				nestedRepeater.Visible = (nestedRepeater.Items.Count > 0);*/
			}
		}
	}
}
