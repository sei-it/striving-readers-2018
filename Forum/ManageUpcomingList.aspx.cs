﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forum_ManageUpcomingList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();

        var events = from upevent in db.UpcomingEvents
                     where upevent.isActive == true
                     select new
                     {
                         upevent.id,
                         upevent.Topic,
                         upevent.Description,
                         upevent.Instructor,
                         upevent.EventDate
                     };
        gvUpcommingEvents.DataSource = events;
        gvUpcommingEvents.DataBind();

    }
    protected void gvUpcommingEvents_Sorting(object sender, EventArgs e)
    {

    }
    protected string getDesShort(int id)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        string shortDes = db.UpcomingEvents.SingleOrDefault(x => x.id == id && x.isActive == true).Description;

        if (string.IsNullOrEmpty(shortDes))
            return "";
        else
        {
            return shortDes.Length >= 250 ? shortDes.Substring(0, 250) + "..." : shortDes;
        }
    }

    protected string getInstructor_Date(int ID)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var item = (from r in db.UpcomingEvents
                    where r.isActive == true &&
                    r.id == ID
                    select r).ToArray();
        string Instruct = item[0].Instructor;
        Instruct = string.IsNullOrEmpty(Instruct) ? "" : Instruct + "<br/>";
        string EDate = item[0].EventDate.Value.ToString("MMMM dd, yyyy");

        return Instruct + EDate;
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        Session["srUpComingId"] = null;
        
        Response.Redirect("ManageUpcomingRecord.aspx");
    }

    protected void onEdit(object sender, EventArgs e)
    {
        int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
        Session["srUpComingId"] = id;
        Response.Redirect("ManageUpcomingRecord.aspx");
    }
}