﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;

namespace aspnetforum
{
    public partial class recenttopics_iphone : ForumPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            recenttopics.BindRepeater(rptTopicsList);
        }
    }
}
