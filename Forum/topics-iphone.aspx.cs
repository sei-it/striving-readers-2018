﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;

namespace aspnetforum
{
	public partial class topics_iphone : ForumPage
	{
		protected int _forumID;
		protected string pagerString = "";
		bool _membersOnly = true;
		bool _restrictTopicCreation;
		protected bool _isModerator = false;
		string _forumTitle;

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				_forumID = int.Parse(Request.QueryString["ForumID"]);
			}
			catch
			{
				Response.Write("Wrong forum id");
				Response.End();
				return;
			}

			_isModerator = IsModerator(_forumID);

			Cn.Open();

			//check permissions
			if (!Utils.Forum.CheckForumReadPermissions(_forumID, CurrentUserID))
			{
				Cn.Close();
				Response.Write("Access to this forum is restricted");
				Response.End();
				return;
			}

			//display the forum title and description, check if anonymous access allowed
			string descr;
			bool premoderated;
			Utils.Forum.GetBasicForumInfo(_forumID, Cn, out _forumTitle, out descr, out _restrictTopicCreation, out premoderated, out _membersOnly);
			Title = _forumTitle;
			MetaDescription = Title;
			MetaKeywords = Title;

			//if the forum is members only - show an error message
			if (_membersOnly && CurrentUserID == 0)
			{
				Cn.Close();
				Response.Write("The forum <strong>\"" + _forumTitle + "\"</strong> is for authenticated users only. Please login or register.");
				return;
			}

			//hide "add post" link if user is a guest OR topic creation is disabled
			if (
				(CurrentUserID != 0 || Utils.Settings.AllowGuestThreads)
				&&
				(!_restrictTopicCreation || _isModerator)
			   )
			{
				linkAddTopic.Visible = true;
				linkAddTopic.HRef = "addpost.aspx?ForumID=" + _forumID;
			}
			else
			{
				linkAddTopic.Visible = false;
			}

			//display subforums of the forum
			topics.BindSubforums(Cmd, rptSubForumsList, _forumID, CurrentUserID);
			
			//topics
			topics.BindTopicsRepeater(Cmd, rptTopicsList, PageSize, Request, out pagerString, _isModerator, _forumID, premoderated);

			Cn.Close();
			
			if (rptTopicsList.Items.Count == 0 && rptSubForumsList.Items.Count == 0)
			{
				Response.Write("The forum is empty, no topics have been created yet.");
				Response.End();
			}
		}
	}
}
