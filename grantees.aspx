﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="grantees.aspx.cs" Inherits="grantees" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Striving Readers - Webinars</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <section class="head-image">
     	<div class="map-back">
            <div class="map-max">

			<div id="map" style="width: 100%"></div>
		</div>
      </div>  
                </section>

                <section class="bg-blue center home-section-pad">
                    <div class="container">
                        <h2>Grantees</h2>
                    </div>
                </section>

				<section class="internal-page-content">
					<div class="container">
					 <div class="col-lg-12 col-xs-12">
					       <h2>Grantee Details</h2>
					       <p>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. Ei admodum temporibus deterruisset usu, duo iuvaret volutpat delicatissimi ut, ut has sanctus rationibus concludaturque. Intellegat eloquentiam adversarium vix no. Mei bonorum definitiones ea, sea an dolor affert laoreet.</p>
					       <div class="scrollme">
					        <!--BEGIN HEAD OF DISPLAY ELEMENTS-->
					        <!--END HEAD OF DISPLAY ELEMENTS-->
                             <asp:Literal ID="ltlGranteesPlaceHolder" runat="server"></asp:Literal>
                             <asp:GridView ID="GridView2" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal"
                        ShowHeaderWhenEmpty="true">
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
                        <Columns>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("State") %>' Style="font-weight: bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField DataField="PublicationTitle" SortExpression="" HeaderText="Title" HeaderStyle-Font-Bold="true" />--%>
                            <asp:BoundField DataField="Abstract" SortExpression="" HeaderText="Abstract" />
                            <asp:TemplateField>
                                <HeaderTemplate>Details</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="GRANTEE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="ShowDetails"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
				          </div>
				      </div>
					  </div>
				</section>	
     <asp:Panel ID="pnlSRgrantee" runat="server">
    <div class="mpeDiv">
            <div class="mpeDivHeader">
                Grantee Details
                <span class="closeit"><asp:LinkButton ID="lbtnClose" runat="server" Text="Close" CssClass="closeit_link" />
                </span>
            </div>
            <div class="popContent">
                <table >
                     <tr>
						<th >Grantee Name:</th>
                        <td>
                            <asp:Literal ID="ltlgranteename" runat="server" />
                        </td>
                    </tr>
					<tr >
						<th >
                            Grantee State:
						</th>
						<td>
                            <asp:Literal ID="ltlGState" runat="server"/>
						</td>
					</tr>
                    <tr>
						<th>
                            Grantee Description:
						</th>
						<td>
                            <asp:Literal ID="ltlDescription" runat="server"/>
						</td>
					</tr>
                     <tr>
						<th>
                            Contact Name:
						</th>
						<td>
                            <asp:Literal ID="ltlContactName" runat="server"/>
						</td>
					</tr>
                    <tr>
                        <th>Contact Email: </th>
                        <td>
                            <asp:Literal ID="ltlEmail" runat="server" />
                        </td>
                     <%-- <tr>
						<th>
                            Contact Phone:
						</th>
						<td>
                            <asp:Literal ID="ltlPhone" runat="server"/>
						</td>
					</tr>--%>
                     <tr>
                         <td colspan="2">
                             <asp:Button ID="btnGrntClose" runat="server" CssClass="msapBtn" Text="Close Window" />
                         </td>
                     </tr>

                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton8" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="mpextGrantee" runat="server" TargetControlID="LinkButton8"
        PopupControlID="pnlSRgrantee" DropShadow="true" OkControlID="btnGrntClose" CancelControlID="btnGrntClose"
        BackgroundCssClass="magnetMPE" Y="20" />   

        <script src="../js/mapdata.js"></script>
		<script src="../js/usmap.js"></script>    
</asp:Content>



