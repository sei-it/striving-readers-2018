﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="srResources.aspx.cs" Inherits="srResources" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Striving Readers - Webinars</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <section class="head-image">
        <img src="images/resources.jpg" class="img-responsive" alt="Resources Computer with Hands" />
    </section>

    <section class="bg-blue center home-section-pad">
        <div class="container">
            <h2>Resources</h2>
        </div>
    </section>

    <section class="internal-page-content">
        <div class="container">
         
         <div class="row">
          <p>The SRCL CoP is dedicated to providing high-quality, evidence-based resources to help develop, implement, and evaluate innovative comprehensive literacy programs. To browse resources by topic, select a topic area in the list box menu below and click Search. To search by keyword, enter the keywords in the Search Terms box and click Search.  The resources can also be accessed by type, which includes Doing What Works (DWW) Adolescent Literacy, DWW Reading Comprehension, DWW RTI Reading, DWW Writing, IES Practice Guides, Individual Studies, and WWC Intervention Reports . </p> 
          
			 <p>To browse resources by type use the Resource Type dropdown menu below. To search by keyword, enter the keywords in the Search Terms box and click Search. Or, to search by topic, select a topic area in the list box menu below and click Search. You can also combine any of the search options below to further filter your results.</p>
          
          
			</div>  
           
           
         <div class="container newsearch_container formControl">           
            <div class="row">
                

                        
                        <div style="background-color: #C0C0C0; text-align: center; width: 100%">
                            <asp:Literal ID="ltlPagingSummary" runat="server" Visible="true" />
                        </div>
                        <br />
                        <asp:HiddenField ID="hfSelection" runat="server" />
                        <asp:HiddenField ID="hfKeyword" runat="server" />
                        <asp:HiddenField ID="hfTitle" runat="server" />
                        <asp:HiddenField ID="hfOrg" runat="server" />

                        <asp:Panel ID="Panel1" runat="server" DefaultButton="kwdButton">
                             
							<section class="resource-select">
                         
                         
                          <div class="col-lg-12 col-xs-12 padFields">
							  <div class="col-lg-2 col-xs-12"><strong>Search Terms</strong></div>
							  <div class="col-lg-10 col-xs-12"><asp:TextBox ID="txtKeywords" runat="server" CssClass="msapTxt" Width="200px"></asp:TextBox></div>
						  </div>

                          <div class="col-lg-12 col-xs-12 padFields">
							  <div class="col-lg-2 col-xs-12"><strong>Resource Type</strong></div>
                               <div class="col-lg-10 col-xs-12">
                                <asp:DropDownList ID="ddlTypeResource" runat="server" CssClass="msapTxt msapDrop">
                                    <asp:ListItem Value="" Selected="True">Select All</asp:ListItem>
                                    <asp:ListItem Value="print">Publications</asp:ListItem>
                                    <asp:ListItem Value="video">Multimedia</asp:ListItem>
                                    <asp:ListItem Value="toolkit">Toolkits &amp; Guides</asp:ListItem>
                                </asp:DropDownList>
							</div>
							</div>
							
                            
                            <div class="col-lg-12 col-xs-12 padFields">
                            
							<div class="col-lg-2 col-xs-12"><strong>Topic Areas</strong></div>
                             <div class="col-lg-10 col-xs-12">
                                <asp:ListBox CssClass="msapTxt msapDrop" Rows="16" DataTextField="topicname" DataValueField="id" DataSourceID="dsPulicationTopic" SelectionMode="Multiple" ID="LboxTopics" runat="server" AutoPostBack="false"></asp:ListBox>
								</div>
							</div>

							<div class="col-lg-12 col-xs-12 padFields">
                            <asp:Button ID="kwdButton" runat="server" Text="Search" CssClass="msapBtn"
                                OnClick="OnSearch" />
                            <asp:Button ID="LinkButton1" runat="server" CssClass="msapBtn" Text="Clear Search Filters" OnClick="OnClearSearch" />
							</div>
							
							
							</section>
                        </asp:Panel>

                    </div>
                    <!-- NewsSearch Container -->
                
                
                
                
                
                
                
                
                <!-- / columns -->
            </div>
            <div class="col-lg-12 col-xs-12">

                <div class="table-responsive">
                    <!--BEGIN HEAD OF DISPLAY ELEMENTS-->

                    <!--END HEAD OF DISPLAY ELEMENTS-->
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AllowSorting="false" Visible="false"
                        ShowHeader="false" AutoGenerateColumns="false" DataKeyNames="ID" CssClass="msapTbl container"
                        GridLines="None" OnPageIndexChanging="OnPageIndexChanging" PageSize="10"
                        EmptyDataText="Your search did not match any documents"
                        OnRowDataBound="GridView1_RowDataBound">
                        <RowStyle BorderStyle="None" BorderColor="White" />
                        <HeaderStyle BorderStyle="None" BorderColor="White" />
                        <Columns>
                            <asp:BoundField DataField="DisplayData" HtmlEncode="false" SortExpression="" HeaderText="" />
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                    <asp:GridView ID="GridView2" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal"
                        ShowHeaderWhenEmpty="true" OnSelectedIndexChanged="GridView2_SelectedIndexChanged">
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
                        <Columns>
                            <asp:TemplateField HeaderText="Title">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("PublicationTitle") %>' Style="font-weight: bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField DataField="PublicationTitle" SortExpression="" HeaderText="Title" HeaderStyle-Font-Bold="true" />--%>
                            <asp:BoundField DataField="Description" SortExpression="" HeaderText="Summary" />
                            <asp:TemplateField>
                                <HeaderTemplate>Link</HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="MORE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="ShowDetails"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:SqlDataSource ID="dsPulicationTopic" runat="server"
                        ConnectionString="<%$ ConnectionStrings:StrivingReaderConnectionString %>"
                        SelectCommand="SELECT * FROM [SubTopic] order by topicname"></asp:SqlDataSource>

                </div>
            </div>

        </div>
    </section>

    <asp:Panel ID="pnlSRresources" runat="server">
        <div class="mpeDiv">
            <div class="mpeDivHeader">
                Resource Details
                <span class="closeit">
                    <asp:LinkButton ID="lbtnClose" runat="server" Text="Close" CssClass="closeit_link" />
                    
					
                </span>
            </div>
            <div class="popContent">
                <asp:Literal ID="ltlResourceDetails" runat="server"></asp:Literal>
                
                <div class="col-lg-12 col-xs-12">
                <asp:Button ID="btnRscsClose" runat="server" CssClass="msapBtn" Text="Close Window" />
				</div>
            </div>

                        

        </div>
    </asp:Panel>
    <asp:LinkButton ID="LinkButton8" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="mpextResources" runat="server" TargetControlID="LinkButton8"
        PopupControlID="pnlSRresources" DropShadow="true" OkControlID="btnRscsClose" CancelControlID="btnRscsClose"
        BackgroundCssClass="magnetMPE" Y="20" />
</asp:Content>


