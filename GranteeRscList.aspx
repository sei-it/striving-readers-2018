﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="GranteeRscList.aspx.cs" Inherits="GranteeRscList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" Runat="Server">
    <section class="home-features" style="padding-top:300px;">
      <div class="container">
    <div>
        <asp:Button ID="btnNew" runat="server" Text="New" OnClick="btnNew_Click" CssClass="btn btn-grantee" CausesValidation="false" />
    </div>
    <table>
        <tr>
            <th>Grantee:</th>
            <td>
                <asp:DropDownList ID="ddlGrantees" runat="server" AppendDataBoundItems="false"  DataTextField="Grantee" DataValueField="ID" >
                    <asp:ListItem Value="">Select One</asp:ListItem>
                </asp:DropDownList>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlGrantees" InitialValue="" Display="Dynamic" ForeColor="Red" ErrorMessage="This field is required."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <th>Topic:</th>
            <td>
                <asp:DropDownList ID="ddlTopics" runat="server"  AppendDataBoundItems="True" DataSourceID="SqlDataSource2" DataTextField="topicname" DataValueField="id" OnDataBound="ddlTopics_DataBound" >
                    <asp:ListItem Value="">Select One</asp:ListItem>
                </asp:DropDownList>
                
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:StrivingReaderConnectionString %>" SelectCommand="SELECT [id], [topicname] FROM [GranteeTopic]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn btn-grantee" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvGranteeRsc" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal" AllowSorting="true" 
                        ShowHeaderWhenEmpty="true" OnSorting="gvGranteeRsc_Sorting"  >
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
        
                        <Columns>
                             <asp:TemplateField HeaderText="Grantee" SortExpression="Grantee">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrantee" runat="server" Text='<%#Eval("Grantee")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Resource" SortExpression="ResourceName">
                                <ItemTemplate>
                                    <asp:Label ID="lblResourceName" runat="server" Text='<%#Eval("ResourceName")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:BoundField DataField="topicname" SortExpression="" HeaderText="Topic" />
                            <asp:BoundField HeaderText="Uploaded Files" DataField="FilePath" HtmlEncode="false" />
                            <asp:TemplateField HeaderText="Approved" runat="server" SortExpression="isActive">
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%#Eval("isActive")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="onEdit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
          </div>
                </section>   
</asp:Content>

