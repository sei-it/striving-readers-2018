﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="webinars-archives.aspx.cs" Inherits="webinars_archives" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Striving Readers - Webinars</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="srContents" runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release"></asp:ScriptManager>
    <section class="head-image">
        <img src="images/webinar-banner.jpg" class="img-responsive" alt="Group people attending a webinar" />
    </section>

    <section class="bg-blue center home-section-pad">
        <div class="container">
            <h2>Archived CoP Events</h2>
        </div>
    </section>

    <section class="internal-page-content">
        <div class="container">
            <div class="col-lg-12 col-xs-12">
                <h2>Event Information</h2>
                <p>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. Ei admodum temporibus deterruisset usu, duo iuvaret volutpat delicatissimi ut, ut has sanctus rationibus concludaturque. Intellegat eloquentiam adversarium vix no. Mei bonorum definitiones ea, sea an dolor affert laoreet.</p>
                <asp:GridView ID="gvArchiveEvent" runat="server" BackColor="White" EmptyDataText="No Records Found" Width="100%"
                        DataKeyNames="ID" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="8" ForeColor="Black" GridLines="Horizontal" AllowSorting="true" 
                        ShowHeaderWhenEmpty="true" OnSorting="gvArchiveEvent_Sorting"  >
                        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                        <HeaderStyle BackColor="#0d2338" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#242121" />
        
                        <Columns>
                             <asp:BoundField DataField="EventName" SortExpression="" HeaderText="Event" />
                            <asp:TemplateField HeaderText="Details" runat="server" SortExpression="">
                                <ItemTemplate>
                                     <asp:Label ID="lblResourceName" runat="server" Text='<%# getDesShort((int)Eval("id")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Date" DataField="EventStartDate" HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />
                            <asp:TemplateField HeaderText="Link">
                                <ItemTemplate >
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="MORE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='<%# Eval("id") %>'
                                        OnClick="ShowDetails"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                <%--<div class="table-responsive">
                    <!--BEGIN HEAD OF DISPLAY ELEMENTS-->


                    <!--END HEAD OF DISPLAY ELEMENTS-->


                    <div class="scrollme">
                        <table class="table table-responsive">
                            <tbody>
                                <tr class="bg-blue">
                                    <th scope="col">Event</th>
                                    <th scope="col">Details</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Link</th>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td>10/15/2017</td>
                                    <td>
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="MORE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='1'
                                        OnClick="ShowDetails" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td>10/15/2016</td>
                                    <td><asp:LinkButton ID="LinkButton2" runat="server" Text="MORE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='2'
                                        OnClick="ShowDetails" /></td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td>10/20/2015</td>
                                    <td>
                                        <asp:LinkButton ID="LinkButton3" runat="server" Text="MORE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='3'
                                        OnClick="ShowDetails" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td>9/15/2014</td>
                                    <td>
                                        <asp:LinkButton ID="LinkButton4" runat="server" Text="MORE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='4'
                                        OnClick="ShowDetails" />
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Lorem ipsum</th>
                                    <td>Lorem ipsum dolor sit amet, alii definitionem vix ne. Has ancillae constituam ei, at has prima dolor conceptam. </td>
                                    <td>5/15/2013</td>
                                    <td>
                                        <asp:LinkButton ID="LinkButton5" runat="server" Text="MORE DETAILS" CssClass="btn btn-grantee" CausesValidation="false" CommandArgument='5'
                                        OnClick="ShowDetails" />
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>--%>
            </div>

        </div>
    </section>
</asp:Content>

