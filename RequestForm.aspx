﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StrivingReadersMasterPage.master" AutoEventWireup="true" CodeFile="RequestForm.aspx.cs" Inherits="RequestForm" enableEventValidation="false"%>
<%@ Register Assembly="BotDetect" Namespace="BotDetect.Web.UI" TagPrefix="BotDetect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <link rel="stylesheet" href="styles/forms.css" />
      <link rel="stylesheet" href="/../code.jquery.com/ui/1.11.0/themes/redmond/jquery-ui.css" />
       <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js" type="text/javascript"></script>
    <script src="js/jquery.selectboxes.min.js" type="text/javascript"></script>
    <script src="js/JsMask.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="srContents" Runat="Server">
                    <section class="bg-blue center home-section-pad form-top">
                    <div class="container">
                        <h2>Technical Assistance (TA)</h2>
                    </div>
                </section> 
                
     <section class="internal-page-content">
     
      <div class="container">
      
		  <p>We understand that each SRCL grantee and subgrantee develops at its own pace and operates within the unique context of its state education or local education agency. At any time, you may have different informational and resource needs. To accommodate these differences, we will use a four-tiered approach to design and deliver Technical Assistance (TA). </p>
     
		  <p class="center"><strong>GLOBAL SERVICES</strong></p>
     
		  <p>These TA services reach the whole SRCL community through the SRCL CoP website, where grantees and subgrantees can find professional learning events and resources, evidence -based information, and tools from a variety of sources.</p>
     
      <p class="center"><strong>INDIVIDUAL SERVICES</strong></p>
      
		  <p>Through one-on-one assistance from expert consultants and the SRCL TA liaisons, an individual SCRL grantee may receive customized support to help design, implement manage and evaluate its SRCL program.</p>
		  
		  
		 <p class="center"><strong>TARGETED SERVICES</strong></p> 
    
		  <p>These services focus on specific groups of SRCL staff, such as project directors, school administrators, and teachers, who each play different roles within their SRCL programs. These services may be delivered at face-to-face meetings and professional learning webinars.</p>
		  
		  <p class="center"><strong>INTENSIVE SERVICES</strong></p> 
		  
		  <p>These services provide assistance with specific comprehensive literacy content to groups of grantees and subgrantees. TA may include sessions with expert consultants in combination with resources created by the SRCL TA liaisons, such as webinars and content-focused tools.</p>
		  
		  <p>To request TA, simply fill in the fields below. Required fields are marked with an asterisk (*). Include your preferred contact method and the best times you can be reached in case someone from our staff needs to speak with you directly. All Technical Assistance requests will be responded to within 3 business days.  </p>
		  
     
      
    <!--  <h3 class="padFields">Technical Assistance (TA)</h3>-->
      

    <div class="formControl">
       	<section>
       	
       		<div class="col-lg-12 col-xs-12 padFields">	
			<div class="col-lg-2 col-xs-12"><label for="txtName">Name</label></div>   
			<div class="col-lg-8 col-xs-12"><asp:textbox id="txtName" runat="server"  ></asp:textbox><span style="color: #FF0000">*</span></div> 
			<div class="col-lg-2 col-xs-12"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" runat="server" ErrorMessage="Required" ForeColor="Red" ControlToValidate="txtName"></asp:RequiredFieldValidator></div>
			</div>
	       
	        <div class="col-lg-12 col-xs-12 padFields">	
				<div class="col-lg-2 col-xs-12"><label for="txtTitle">Title</label></div>
				<div class="col-lg-8 col-xs-12"><asp:textbox id="txtTitle" runat="server" ></asp:textbox><span style="color: #FF0000">*</span></div>
				<div class="col-lg-2 col-xs-12"><asp:RequiredFieldValidator ID="RequiredFieldValidator2a" ForeColor="Red" SetFocusOnError="true" runat="server" ErrorMessage="Required" ControlToValidate="txtTitle"></asp:RequiredFieldValidator></div>
			</div>
           
           
           
            <div class="col-lg-12 col-xs-12 padFields">	
				<div class="col-lg-2 col-xs-12"><label for="txtEMail">Email address </label></div>
				<div class="col-lg-8 col-xs-12"><asp:textbox id="txtEMail" runat="server" ></asp:textbox><span style="color: #FF0000">*</span></div>
                <div class="col-lg-2 col-xs-12"><asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" runat="server" SetFocusOnError="true" ErrorMessage="Required" ControlToValidate="txtEMail"></asp:RequiredFieldValidator>
              
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              
                <asp:RegularExpressionValidator ID="revEmailadd" runat="server" ErrorMessage=" Email address is not formatted correctly" SetFocusOnError="true"
                                ForeColor="Red" ControlToValidate="txtEMail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
				</div>  
                    
			</div>
	        
	        
	        
	        <div class="col-lg-12 col-xs-12 padFields">	
				<div class="col-lg-2 col-xs-12"><label for="ddlSEA">SEA </label></div>
		        
		        <div class="col-lg-8 col-xs-12"><asp:DropDownList id="ddlSEA" runat="server" AppendDataBoundItems="True" DataSourceID="dsSEA" DataTextField="sea" DataValueField="id"     > 
                    <asp:ListItem Value="">Select One</asp:ListItem>
		        </asp:DropDownList>
				</div>
               
               
                <div class="col-lg-2 col-xs-12"><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlSEA"
                ErrorMessage="Required" ForeColor="red" SetFocusOnError="true" InitialValue=""></asp:RequiredFieldValidator>
	            <asp:SqlDataSource ID="dsSEA" runat="server" ConnectionString="<%$ ConnectionStrings:StrivingReaderConnectionString %>" SelectCommand="SELECT [id], [sea] FROM [TASea]"></asp:SqlDataSource>
				</div>
				
			</div>
           
           
           
           
           
           <div class="col-lg-12 col-xs-12 padFields">	
			   <div class="col-lg-2 col-xs-12"><label for="txtTopic">TA Topic </label></div>
			   <div class="col-lg-8 col-xs-12"><asp:textbox id="txtTopic" runat="server" ></asp:textbox><span style="color: #FF0000">*</span></div>
			   <div class="col-lg-2 col-xs-12"><asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" SetFocusOnError="true" runat="server" ErrorMessage="Required" ControlToValidate="txtTopic"></asp:RequiredFieldValidator></div>
			</div>
	        
	        
	        
	        
	        <div class="col-lg-12 col-xs-12 padFields">	
				<div class="col-lg-2 col-xs-12"><label for="rblOrganizationType">TA Recipient(s) </label></div>		        
                <div class="col-lg-8 col-xs-12"><asp:DropDownList id="ddlRecipients" runat="server" AppendDataBoundItems="True" DataSourceID="dsRecipient" DataTextField="recipient" DataValueField="id"> 
					<asp:ListItem Value="">Select One</asp:ListItem></asp:DropDownList>
					<span style="color: #FF0000">*</span>
				</div>
                
                
                 <div class="col-lg-2 col-xs-12"><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlRecipients"
                ErrorMessage="Required" ForeColor="Red" SetFocusOnError="true" InitialValue=""></asp:RequiredFieldValidator>
                <asp:SqlDataSource ID="dsRecipient" runat="server" ConnectionString="<%$ ConnectionStrings:StrivingReaderConnectionString %>" SelectCommand="SELECT [id], [recipient] FROM [TARecipients]"></asp:SqlDataSource>
                <asp:HiddenField ID="hfRecipients" runat="server" />
				</div>

			</div>
           
           
           
           
            <div class="col-lg-12 col-xs-12 padFields">	
				<div class="col-lg-2 col-xs-12"><label for="ddlCapacityArea" style="width:auto;">Capacity Area TA will Address </label></div>
               		        
                <div class="col-lg-8 col-xs-12"><asp:DropDownList id="ddlCapacityArea" runat="server" AppendDataBoundItems="True" DataSourceID="dsCapacity" DataTextField="title" DataValueField="id"    > 
                    <asp:ListItem Value="">Select One</asp:ListItem>
		        </asp:DropDownList><span style="color: #FF0000">*</span>
				</div>
               
                <div class="col-lg-2 col-xs-12"><asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlCapacityArea"
                ErrorMessage="Required" ForeColor="Red" SetFocusOnError="true"  InitialValue=""></asp:RequiredFieldValidator>
                <asp:SqlDataSource ID="dsCapacity" runat="server" ConnectionString="<%$ ConnectionStrings:StrivingReaderConnectionString %>" SelectCommand="SELECT [id], [title], [details] FROM [CapacityArea]"></asp:SqlDataSource>
                <asp:HiddenField ID="hfCapacityArea" runat="server" />
				</div>

			</div>
	        
	        
	       
	        <div class="col-lg-12 col-xs-12 padFields">	
				<div class="col-lg-12 col-xs-12"><label for="txtExplanation">Explanation of Need</label>
                <p>
                    <i>Describe current and previous work of your agency, resources reviewed, and TA received regarding requested topic. Describe your agency’s current request for TA within the context of past and ongoing efforts</i>
                </p>
				</div>
	        <div class="col-lg-7 col-xs-12">
		        <asp:textbox id="txtExplanation" runat="server" TextMode="MultiLine" Height="129px" ></asp:textbox><span style="color: #FF0000">*</span>
				</div>
               <div class="col-lg-5 col-xs-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ForeColor="Red" SetFocusOnError="true"  ErrorMessage="Required" ControlToValidate="txtExplanation"></asp:RequiredFieldValidator>
				</div>
			</div>
           
           
           
           
            <div class="col-lg-12 col-xs-12 padFields">	
            <div class="col-lg-12 col-xs-12">
		        <label for="txtAlignment" style="width:auto;">TA Plan Goal Alignment</label>
               <br /><i> Please state:
                <ul>
                    <li>
                        Which goal (s) in the individualized TA plan developed  for your state does this request address?
                    </li>
                    <li>
                        How will the TA request assist you to meet this goal(s)?
                    </li>
                </ul>
                </i>
				</div>
	        
	        <div class="col-lg-7 col-xs-12">
		        <asp:textbox id="txtAlignment" runat="server"  TextMode="MultiLine" placeholder="Goals(s) &#10; 1. &#10; 2. &#10;&#10; How will the TA request assist in meeting this goal/s?"  Height="260px" ></asp:textbox><span style="color: #FF0000">*</span>
				</div>
               <div class="col-lg-5 col-xs-12">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" SetFocusOnError="true"  runat="server" ErrorMessage="Required" ControlToValidate="txtAlignment"></asp:RequiredFieldValidator>
				</div>
			</div>

            <div class="col-lg-12 col-xs-12 padFields">
                <div class="col-lg-12 col-xs-12">
                    <label for="CaptchaCode" ID="CaptchaLabel" runat="server" AssociatedControlID="CaptchaCode">Retype the characters from the picture:</label></div>
                <BotDetect:WebFormsCaptcha ID="ExampleCaptcha" runat="server" /><br /><br />
                <asp:TextBox ID="CaptchaCode" runat="server" />
                <asp:Label ID="CaptchaErrorLabel" runat="server" />
                <asp:CustomValidator runat="server" ID="CaptchaValidator"
                    ControlToValidate="CaptchaCode" SetFocusOnError="true" ForeColor="Red"
                    ErrorMessage="Incorrect CAPTCHA code!"
                    OnServerValidate="CaptchaValidator_ServerValidate" />
            </div>

            <div class="col-lg-12 col-xs-12">
        <asp:Button ID="btnSave" runat="server" Text="Submit " onclick="btnSave_Click"  TabIndex="36"/>
			</div>        
	        
	        
			</section>
		 </div>
      
	</div>
	<asp:HiddenField ID="hfID" runat="server" />
	</section>
</asp:Content>
